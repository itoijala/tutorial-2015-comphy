#!/usr/bin/env python3

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

def f(r, x, xp):
    return (r * x * (1 - x),
                x * (1 - x) + r * (1 - 2 * x) * xp)

def g(n, r):
    x, xp = 0.5, 0
    for _ in range(2**n):
        x, xp = f(r, x, xp)
    return 0.5 - x, -xp

N = 15

R = np.empty(N)
R[[0, 1]] = 2, 1 + np.sqrt(5)

delta = np.empty(N)
delta[[0, 1]] = 0, 5

def newton(f, x0, epsilon):
    while True:
        y, yp = f(x0)

        x1 = x0 - y / yp

        x0 = x1

        if np.abs(x1 - x0) < epsilon * np.abs(x1 + x0):
            break

    return x0

for n in range(2, N):
    R0 = R[n - 1] + (R[n - 1] - R[n - 2]) / delta[n - 1]
    R[n] = newton(lambda r: g(N, r), R0, 1e-10)
    delta[n] = (R[n - 1] - R[n - 2]) / (R[n] - R[n - 1])

print('# n root Feigenbaum')
print(np.hstack((np.arange(N)[:, np.newaxis], R[:, np.newaxis], delta[:, np.newaxis])))
print('δ_lit =', 4.669201609)
