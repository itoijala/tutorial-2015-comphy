all: 1-logistic.png 1-cubic.png \
     2a.pdf 2d.txt

1-%.png: 1.py
	./$< $*

2a.pdf: 2a.py
	./$< | tee 2a.txt

2d.txt: 2d.py
	./$< | tee 2d.txt

CC := g++
CXX := g++
INCLUDES := $(shell pkg-config --cflags eigen3 | sed "s/-I/-isystem/g")

# https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html
WARNINGS := -Wpedantic -Wall -Wextra \
            -Wformat=2 \
            -Wmissing-include-dirs \
            -Wuninitialized \
            -Wtrampolines \
            -Wfloat-equal \
            -Wundef \
            -Wcast-qual \
            -Wcast-align \
            -Wconversion \
            -Wzero-as-null-pointer-constant \
            -Wuseless-cast \
            -Wenum-compare \
            -Wsign-conversion \
            -Wlogical-op \
            -Wno-aggressive-loop-optimizations \
            -Wopenmp-simd \
            -Wpacked \
            -Wredundant-decls \
            -Winline \
            -Wvector-operation-performance \
            -Wdisabled-optimization \
            -Wno-unused-parameter \

CXXFLAGS := -std=c++11 -fopenmp -g -O2 $(INCLUDES) $(WARNINGS)
LDFLAGS := -fopenmp
LDLIBS :=

SHEET := $(shell basename $$(pwd))

$(SHEET).tar.xz: all
	bash -O nullglob -c 'tar -cvJf $@ $(addprefix ../$(SHEET)/, .gitignore Makefile matplotlibrc *.cpp *.hpp *.h *.py *.txt *.pdf *.png *.mp4)'

dist: $(SHEET).tar.xz

clean:
	rm -f $(shell cat .gitignore) *.o *.dummy *.dat *.txt *.pdf *.png *.mp4 *.xz

.SECONDARY:

.PHONY: all dist clean
