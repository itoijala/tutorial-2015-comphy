#!/usr/bin/env python3

import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

funcs = {
    'logistic': {
        'func': lambda r, x: r * x * (1 - x),
        'r_min': 0,
        'r_max': 4,
        'x0': [0.5],
    },
    'cubic': {
        'func': lambda r, x: r * x - x**3,
        'r_min': 0,
        'r_max': 3,
        'x0': [-0.5, 0.5],
    },
}

N_r = int(2e3)
N_start = int(1e4)
N_plot = int(1e3)

func, r_min, r_max, x0 = [funcs[sys.argv[1]][k] for k in ['func', 'r_min', 'r_max', 'x0']]

r = np.tile(np.linspace(r_min, r_max, N_r), len(x0))
x = np.repeat(x0, N_r)

for _ in range(N_start):
    x = func(r, x)

x, x[0] = np.empty((N_plot, len(x))), x

for n in range(1, N_plot):
    x[n] = func(r, x[n - 1])

r = np.tile(r, (N_plot, 1))

fig, ax = plt.subplots(1, 1)

ax.plot(r.flatten(), x.flatten(), ',')

ax.set_xlabel(r'$r$')
ax.set_ylabel(r'$x$')

fig.savefig('1-{}.png'.format(sys.argv[1]), dpi=200)
