#!/usr/bin/env python3

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

def f(r, x):
    return r * x * (1 - x)

def g(n, r):
    x = 0.5
    for _ in range(2**n):
        x = f(r, x)
    return 0.5 - x

r_inf = 3.57
N = 3

r = np.linspace(0, r_inf, 1000)

ads = np.abs(np.diff(np.sign(g(N, r))))

left2 = np.where(ads == 2)[0]
right2 = left2 + 1

left1 = np.where(ads == 1)[0][::2]
right1 = left1 + 2

left = r[np.sort(np.concatenate((left2, left1)))]
right = r[np.sort(np.concatenate((right2, right1)))]

def regula_falsi(f, a, c, epsilon):
    side = 0
    fa = f(a)
    fc = f(c)

    while True:
        if np.abs(c - a) < epsilon * np.abs(c + a):
            break

        b = (a * fc - c * fa) / (fc - fa)
        fb = f(b)

        if fb * fc > 0:
            c = b
            fc = fb
            if side == -1:
                fa /= 2
            side = -1
        elif fb * fa > 0:
            a = b
            fa = fb
            if side == +1:
                fc /= 2
            side = +1
        else:
            break

    return b

r0 = np.empty(len(left))
for n in range(len(r0)):
    r0[n] = regula_falsi(lambda r: g(N, r), left[n], right[n], 1e-7)

delta = (r0[-2] - r0[-3]) / (r0[-1] - r0[-2])

print('# left right root')
print(np.hstack((left[:, np.newaxis], right[:, np.newaxis], r0[:, np.newaxis])))
print('δ =', delta)

fig, (ax, *axs) = plt.subplots(N + 2, 1)
fig.set_figheight((N + 2) * fig.get_figheight())

ax.axhline(y=0, c='k')

for n in range(1, N + 1):
    ax.plot(r, g(n, r), label=r'$n = {}$'.format(n))

ax.legend(loc='best')
ax.set_xlabel(r'$r$')
ax.set_ylabel(r'$g_n(r)$')

for n in range(len(left)):
    r = np.linspace(left[n], right[n])

    axs[n].axhline(y=0, c='k')

    axs[n].plot(r, g(N, r), c='b')

    axs[n].axvline(x=r0[n], c='r')

    axs[n].set_xlabel(r'$r$')
    axs[n].set_ylabel(r'$g_{}(r)$'.format(N))

fig.savefig('2a.pdf')
