#include <cctype>
#include <cmath>
#include <cstdio>
#include <random>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

using A = std::vector<bool>;

int spin(const bool s)
{
	return 2 * s - 1;
}

bool repr(const int s)
{
	return s > 0;
}

void flip(A& a, const size_t i)
{
	a[i] = !a[i];
}

int E_spin(const A& a, const size_t Nx, const size_t Ny, const size_t x, const size_t y)
{
	return - spin(a[Nx * x + y]) * (  spin(a[Nx * ((x - 1 + Nx) % Nx) + y])
	                                + spin(a[Nx * ((x + 1 + Nx) % Nx) + y])
	                                + spin(a[Nx * x + ((y - 1 + Ny) % Ny)])
	                                + spin(a[Nx * x + ((y + 1 + Ny) % Ny)]));
}

int E_total(const A& a, const size_t Nx, const size_t Ny)
{
	int E = 0.;
	for (size_t x = 0; x < Nx; ++x)
	{
		for (size_t y = 0; y < Ny; ++y)
		{
			E += - spin(a[Nx * x + y]) * (  spin(a[Nx * ((x + 1 + Nx) % Nx) + y])
			                              + spin(a[Nx * x + ((y + 1 + Ny) % Ny)]));
		}
	}
	return E;
}

int M_total(const A& a, const size_t Nx, const size_t Ny)
{
	int M = 0;
	for (size_t i = 0; i < a.size(); ++i)
	{
		M += spin(a[i]);
	}
	return M;
}

template <class URNG>
std::tuple<int, int> mc_sweep(A& a, const size_t Nx, const size_t Ny, const double T, URNG& gen)
{
	double boltzmann[] = {exp(-4. / T),
	                      exp(-8. / T)};

	std::uniform_int_distribution<size_t> dist_x(0, Nx - 1);
	std::uniform_int_distribution<size_t> dist_y(0, Ny - 1);
	std::uniform_real_distribution<double> dist_p(0., 1.);

	int dE_total = 0;
	int dM_total = 0;

	for (size_t i = 0; i < Nx * Ny; ++i)
	{
		const size_t x = dist_x(gen);
		const size_t y = dist_y(gen);
		const int dE = -2 * E_spin(a, Nx, Ny, x, y);

		if (dE <= 0 || dist_p(gen) < boltzmann[dE / 4 - 1])
		{
			flip(a, Nx * x + y);
			dE_total += dE;
			dM_total += 2 * spin(a[Nx * x + y]);
		}
	}

	return std::make_tuple(dE_total, dM_total);
}

struct InitUp
{
	template <class URNG>
	void operator()(A& a, URNG& gen) const
	{
		for (size_t i = 0; i < a.size(); ++i)
		{
			a[i] = repr(1);
		}
	}
};

struct InitRandom
{
	template <class URNG>
	void operator()(A& a, URNG& gen) const
	{
		std::uniform_int_distribution<int> dist(0, 1);
		for (size_t i = 0; i < a.size(); ++i)
		{
			a[i] = repr(2 * dist(gen) - 1);
		}
	}
};

template <class Init>
void work_moment(const size_t N, const double T, const Init& init)
{
	const size_t sweeps = 100;
	std::mt19937 gen;

	A a(N * N);
	init(a, gen);

	for (size_t i = 0; i < sweeps; ++i)
	{
		mc_sweep(a, N, N, T, gen);
	}

	for (size_t x = 0; x < N; ++x)
	{
		for (size_t y = 0; y < N; ++y)
		{
			printf("%+d ", spin(a[N * x + y]));
		}
		printf("\n");
	}
}

template <class Init>
void work_equilibration(const size_t N, const double T, const Init& init)
{
	const size_t sweeps = 1000;
	std::mt19937 gen;

	A a(N * N);
	init(a, gen);

	int E = E_total(a, N, N);
	int M = M_total(a, N, N);

	printf("# t E M\n");
	printf("0 %.20e %.20e\n", double(E) / pow(double(N), 2), double(M) / pow(double(N), 2));

	for (size_t i = 1; i <= sweeps; ++i)
	{
		int dE, dM;
		std::tie(dE, dM) = mc_sweep(a, N, N, T, gen);
		E += dE;
		M += dM;

		printf("%zu %.20e %.20e\n", i, double(E) / pow(double(N), 2), double(M) / pow(double(N), 2));
	}
}

template <class Init>
void work_average(const size_t N, const double T, const Init& init)
{
	const size_t equilibration = 1e4;
	const size_t sweeps = 1e5;
	std::mt19937 gen;

	A a(N * N);
	init(a, gen);

	int E = E_total(a, N, N);
	int M = M_total(a, N, N);

	for (size_t i = 1; i <= equilibration; ++i)
	{
		int dE, dM;
		std::tie(dE, dM) = mc_sweep(a, N, N, T, gen);
		E += dE;
		M += dM;
	}

	double E_avg  = 0.;
	double E2_avg = 0.;
	double M_avg  = 0.;
	double M2_avg = 0.;
	double M4_avg = 0.;

	for (size_t i = 1; i <= sweeps; ++i)
	{
		int dE, dM;
		std::tie(dE, dM) = mc_sweep(a, N, N, T, gen);
		E += dE;
		M += dM;

		const double E_norm = double(E) / pow(double(N), 2);
		const double M_norm = fabs(M)   / pow(double(N), 2);

		E_avg  += (E_norm         - E_avg ) / double(i);
		E2_avg += (pow(E_norm, 2) - E2_avg) / double(i);
		M_avg  += (M_norm         - M_avg ) / double(i);
		M2_avg += (pow(M_norm, 2) - M2_avg) / double(i);
		M4_avg += (pow(M_norm, 4) - M4_avg) / double(i);

		if (i % (sweeps / 100) == 0)
		{
			fprintf(stderr, "%zu / %zu\n", i, sweeps);
		}
	}

	printf("# T E E² |M| M² M⁴\n");
	printf("%.20e %.20e %.20e %.20e %.20e %.20e\n", T, E_avg, E2_avg, M_avg, M2_avg, M4_avg);
}

double string_to_double(const std::string& s)
{
	std::invalid_argument e(__FUNCTION__ + (": '" + s + "'"));
	size_t i = 0;
	try
	{
		double r = std::stod(s, &i);
		if (i != s.length() || isspace(s[0]))
		{
			throw e;
		}
		return r;
	}
	catch (...)
	{
		throw e;
	}
}

size_t string_to_size_t(const std::string& s)
{
	std::invalid_argument e(__FUNCTION__ + (": '" + s + "'"));
	size_t i = 0;
	try
	{
		size_t r = std::stoull(s, &i);
		if (i != s.length() || isspace(s[0]))
		{
			throw e;
		}
		return r;
	}
	catch (...)
	{
		throw e;
	}
}

int usage(char** argv)
{
	fprintf(stderr, "Usage: %s moment|equilibration|average <N> <T> up|random\n", argv[0]);
	return 1;
}

int main(int argc, char** argv)
{
	if (argc != 5)
	{
		return usage(argv);
	}

	const size_t N = string_to_size_t(argv[2]);
	const double T = string_to_double(argv[3]);

	const std::string arg(argv[1]);
	const std::string i(argv[4]);
	if (arg == "moment")
	{
		if (i == "up")
		{
			work_moment(N, T, InitUp());
		}
		else if (i == "random")
		{
			work_moment(N, T, InitRandom());
		}
		else
		{
			return usage(argv);
		}
	}
	else if (arg == "equilibration")
	{
		if (i == "up")
		{
			work_equilibration(N, T, InitUp());
		}
		else if (i == "random")
		{
			work_equilibration(N, T, InitRandom());
		}
		else
		{
			return usage(argv);
		}
	}
	else if (arg == "average")
	{
		if (i == "up")
		{
			work_average(N, T, InitUp());
		}
		else if (i == "random")
		{
			work_average(N, T, InitRandom());
		}
		else
		{
			return usage(argv);
		}
	}
	else
	{
		return usage(argv);
	}
}
