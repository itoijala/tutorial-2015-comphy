#!/usr/bin/env python3

import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

N = sys.argv[1]
Ts = sys.argv[2:]

fig, (axE, axM) = plt.subplots(2, 1)
fig.set_figheight(2 * fig.get_figheight())

for T in Ts:
    for init, style in [('random', '-'), ('up', '--')]:
        t, E, M = np.loadtxt('2-equilibration-{}-{}-{}.dat'.format(N, T, init), unpack=True)

        axE.plot(t, E, style, label=r'{} $T = {}$'.format(init, T))
        axM.plot(t, M, style, label=r'{} $T = {}$'.format(init, T))

axE.set_ylim(-2.2, -0.5)

axE.set_xlabel(r'$t$')
axE.set_ylabel(r'$E$')

axM.set_xlabel(r'$t$')
axM.set_ylabel(r'$M$')

axE.legend(loc='best')
axM.legend(loc='best')

fig.savefig('2-equilibration-{}-{}.pdf'.format(N, '-'.join(Ts)))
