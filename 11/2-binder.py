#!/usr/bin/env python3

import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

init = sys.argv[1]
Ts = sys.argv[2]
Ns = sys.argv[3:]

TC = 2 / np.log(1 + np.sqrt(2))

fig, (ax2, ax4, axU) = plt.subplots(3, 1)
fig.set_figheight(3 * fig.get_figheight())

for N in Ns:
    T, E, E2, M, M2, M4 = np.loadtxt('2-averages-{}-{}-{}.dat'.format(N, init, Ts), unpack=True, skiprows=1)

    U = 1 - M4 / (3 * M2**2)

    ax2.plot(T, M2, 'x-', label=r'$N = {}$'.format(N))
    ax4.plot(T, M4, 'x-', label=r'$N = {}$'.format(N))
    axU.plot(T, U, 'x-', label=r'$N = {}$'.format(N))

for ax in [ax2, ax4, axU]:
    ax.axvline(x=TC, c='r', ls='--', zorder=0)
    ax.set_xlabel(r'$T$')
    ax.legend(loc='best')

ax2.set_ylabel(r'$M^2$')
ax4.set_ylabel(r'$M^4$')
axU.set_ylabel(r'$U$')

fig.savefig('2-binder-{}_{}_{}.pdf'.format(init, Ts, '-'.join(Ns)))
