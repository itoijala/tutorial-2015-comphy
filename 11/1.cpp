#include <cmath>
#include <cstdio>
#include <random>

template <class URNG>
double metropolis(const double H, const size_t N, URNG& gen)
{
	const double boltzmann = exp(-2. * fabs(H));
	std::uniform_real_distribution<> dist;
	const bool H_negative = std::signbit(H);

	double m = 0.;
	int s = 1.;

	for (size_t i = 1; i <= N; ++i)
	{
		if (((s > 0) == H_negative) || dist(gen) < boltzmann)
		{
			s*= -1;
		}

		m += (double(s) - m) / double(i);
	}

	return m;
}

void work(const double H_min, const double H_max, const size_t P)
{
	std::mt19937 gen;

	auto file = fopen("1.dat", "w");
	fprintf(file, "# H/T m\n");

	for (size_t i = 0; i < P; ++i)
	{
		const double H = H_min + (H_max - H_min) / double(P - 1) * double(i);
		const double m = metropolis(H, size_t(1e5), gen);

		fprintf(file, "%.20e %.20e\n", H, m);
	}

	fclose(file);
}

int main(int argc, char** argv)
{
	work(-5., +5., 1000);
}
