#!/usr/bin/env python3

import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

a = np.loadtxt(sys.argv[1])

fig, ax = plt.subplots(1, 1)

ax.imshow(a, interpolation='none')

fig.savefig('{}.pdf'.format(sys.argv[1].rsplit('.', 1)[0]))
