#!/usr/bin/env python3

import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

T, E, E2, M, M2, M4 = np.loadtxt(sys.argv[1], unpack=True, skiprows=1)
C = (E2 - E**2) / T**2
TC = 2 / np.log(1 + np.sqrt(2))

fig, (axE, axM, axC) = plt.subplots(3, 1)
fig.set_figheight(3 * fig.get_figheight())

axE.plot(T, E, 'x-')
axE.axvline(x=TC, c='r', ls='--', zorder=0)

axE.set_ylim(-2.05, 0)

axE.set_xlabel(r'$T$')
axE.set_ylabel(r'$E$')

axM.plot(T, M, 'x-')
axM.axvline(x=TC, c='r', ls='--', zorder=0)

axM.set_ylim(0, 1.05)

axM.set_xlabel(r'$T$')
axM.set_ylabel(r'$|M|$')

axC.plot(T, C, 'x-')
axC.axvline(x=TC, c='r', ls='--', zorder=0)

axC.set_xlabel(r'$T$')
axC.set_ylabel(r'$C$')

fig.savefig('{}.pdf'.format(sys.argv[1].rsplit('.', 1)[0]))
