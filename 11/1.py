#!/usr/bin/env python3

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

H, m = np.loadtxt('1.dat', unpack=True)

fig, ax = plt.subplots(1, 1)

ax.plot(H, m, 'b-', label=r'numeric')

x = np.linspace(np.min(H), np.max(H), 1000)
ax.plot(x, np.tanh(x), 'r--', label=r'$\tanh(H/T)$')

ax.set_xlabel(r'$H/T$')
ax.set_ylabel(r'$m$')

ax.legend(loc='best')

ax.set_xlim(np.min(H), np.max(H))
ax.set_ylim(-1.1, 1.1)

fig.savefig('1.pdf')
