#!/usr/bin/env python3

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

def plot(name):
    t, y, exact, error = np.loadtxt('{}.dat'.format(name), unpack=True)

    fig, (ax1, ax2) = plt.subplots(1, 2)

    ax1.plot(t, np.abs(y), label='Euler')
    ax1.plot(t, np.abs(exact), label='exact')

    ax2.plot(t, np.abs(error))

    ax1.set_yscale('log')
    ax2.set_yscale('log')
    ax1.legend(loc='best')
    ax1.set_xlabel('$t$')
    ax1.set_ylabel('$|y|$')
    ax2.set_xlabel('$t$')
    ax2.set_ylabel('$|(e(t) - y) / e(t)|$')
    fig.savefig('{}.pdf'.format(name))

def a():
    plot('2a')

def b():
    plot('2b')

if __name__ == '__main__':
    a()
    b()
