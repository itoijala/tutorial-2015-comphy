#!/usr/bin/env python3

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import sympy

def lambdify(f):
    y = sympy.symbols('x')
    f = f(y)
    @np.vectorize
    def g(x):
        return f.evalf(1000, subs={y: x})
    return g

def work(name, x, naive, clever, exact):
    exact = lambdify(exact)
    exact = exact(x).astype(x.dtype.type)

    fig, (ax1, ax2) = plt.subplots(1, 2)

    ax1.plot(x, naive(x), label='naive')
    ax1.plot(x, clever(x), label='clever')
    ax1.plot(x, exact, label='exact')

    ax2.plot(x, (exact - naive(x)) / exact, label='naive')
    ax2.plot(x, (exact - clever(x)) / exact, label='clever')

    ax1.legend(loc='best')
    ax2.legend(loc='best')
    ax1.xaxis.get_major_formatter().set_powerlimits((0, 0))
    ax1.yaxis.get_major_formatter().set_powerlimits((0, 0))
    ax2.xaxis.get_major_formatter().set_powerlimits((0, 0))
    ax2.yaxis.get_major_formatter().set_powerlimits((0, 0))
    ax1.set_xlim(x[0], x[-1])
    ax2.set_xlim(x[0], x[-1])
    ax1.set_xlabel('$x$')
    ax1.set_ylabel('$f(x)$')
    ax2.set_xlabel('$x$')
    ax2.set_ylabel('$(e(x) - f(x)) / e(x)$')
    fig.savefig('{}.pdf'.format(name))

def a():
    naive = lambda x: 1 / np.sqrt(x) - 1 / np.sqrt(x + 1)
    # Puiseux series
    clever = lambda x: 1/2 / x**(3/2) - 3/8 / x**(5/2) + 5/16 / x**(7/2) - 35/128 / x**(9/2)
    clever = lambda x: 1 / ((np.sqrt(x + 1) + np.sqrt(x)) * np.sqrt(x * (x + 1)))
    exact = lambda x: 1 / sympy.sqrt(x) - 1 / sympy.sqrt(x + 1)
    work('1a', np.linspace(1e10, 1e11, 1000, dtype=np.float64), naive, clever, exact)

def b():
    naive = lambda x: (1 - np.cos(x)) / np.sin(x)
    clever = lambda x: 2 * np.sin(x / 2)**2 / np.sin(x)
    exact = lambda x: (1 - sympy.cos(x)) / sympy.sin(x)
    work('1b', np.linspace(-1e-6, 1e-6, 1000, dtype=np.float64), naive, clever, exact)

if __name__ == '__main__':
    a()
    b()
