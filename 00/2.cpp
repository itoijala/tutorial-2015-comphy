#include <cmath>
#include <cstdio>
#include <functional>
#include <tuple>
#include <vector>

using std::function;
using std::tuple;
using std::vector;

tuple<vector<double>, vector<double>> symmetric_euler(const function<double(double, double)>& f, const size_t N, const double t0, const double t_max, const double y0, const double y1)
{
	const double h = (t_max - t0) / (N - 1);

	vector<double> t(N);
	t[0] = t0;
	t[1] = t0 + h;

	vector<double> y(N);
	y[0] = y0;
	y[1] = y1;

	for (size_t i = 2; i < N; ++i)
	{
		t[i] = i * h;
		y[i] = y[i - 2] + 2 * h * f(t[i - 1], y[i - 1]);
	}

	return std::make_tuple(t, y);
}

void work(const function<double(double, double)>& f, const function<double(double)>& exact, const std::string& name)
{
	const size_t N = 1e5;
	const double t_max = 100.;

	vector<double> t;
	vector<double> y;
	std::tie(t, y) = symmetric_euler(f, N, 0., t_max, 1., 1. - t_max / (N - 1));

	auto file = fopen((name + ".dat").c_str(), "w");
	fprintf(file, "# t y exact error\n");
	for (size_t i = 0; i < N; ++i)
	{
		double e = exact(t[i]);
		double error = (e - y[i]) / e;
		fprintf(file, "%.20f %.20f %.20f %.20f\n", t[i], y[i], e, error);
	}
	fclose(file);
}

void a()
{
	work([] (double t, double y) { return -y; }, [] (double t) { return exp(-t); }, "2a");
}

void b()
{
	work([] (double t, double y) { return +y; }, [] (double t) { return exp(+t); }, "2b");
}

int main()
{
	a();
	b();
}
