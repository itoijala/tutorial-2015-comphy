#!/usr/bin/env python3

import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

φ = np.loadtxt(sys.argv[1])

fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
fig.set_figheight(3 * fig.get_figheight())

x = np.linspace(0, 1, len(φ))
y = np.linspace(0, 1, len(φ))

φ_max = np.max(np.abs(φ))
if φ_max == 0:
    φ_max = 1

ax1.imshow(φ, origin='upper', interpolation='none', vmin=-φ_max, vmax=φ_max, cmap='bwr', extent=[0, 1, 1, 0])

ax1.set_title(r'$\phi$')
ax1.set_xlabel(r'$x$')
ax1.set_ylabel(r'$y$')

h = 1 / len(φ)
E = - np.array(np.gradient(φ, h))
Ey, Ex = E

norm_E = np.sqrt(Ex**2 + Ey**2)

step = len(φ) / 32

ax2.quiver(x[::step], y[::step], Ex[::step, ::step], Ey[::step, ::step], angles='xy', pivot='middle')
ax2.imshow(norm_E, origin='upper', interpolation='none', cmap='Blues', extent=[0, 1, 1, 0])

ax2.set_title(r'$E$')
ax2.set_xlabel(r'$x$')
ax2.set_ylabel(r'$y$')

θ = np.concatenate([
        np.fmod(np.arctan2(Ey[0, :],  Ex[0, :]),              np.pi),
        np.fmod(np.arctan2(Ey[:, -1], Ex[:, -1]) - np.pi / 2, np.pi),
        np.fmod(np.arctan2(Ey[-1, :], Ex[-1, :]),             np.pi),
        np.fmod(np.arctan2(Ey[:, 0],  Ex[:, 0])  - np.pi / 2, np.pi),
        ])

ax3.plot(np.linspace(0, 4, 4 * len(φ)), θ)

ax3.set_xlabel(r'$l$')
ax3.set_ylabel(r'$\theta$')

σ = h * np.sum(np.concatenate([
        +Ey[0,  :],
        -Ex[:, -1],
        -Ey[-1, :],
        +Ex[:,  0],
        ]))

print('σ = {}'.format(σ))

fig.savefig('{}.pdf'.format(sys.argv[1].rsplit('.', 1)[0]))
