#!/usr/bin/env python3

import sys

import matplotlib as mpl
mpl.rcdefaults()
import matplotlib.animation
import matplotlib.pyplot as plt
import numpy as np

data = np.loadtxt(sys.argv[1], unpack=True)
t = data[0]
psi = data[1:].T
x = np.linspace(-10, 10, len(psi[0]))

fig, ax = plt.subplots(1, 1)
ax.set_xlim(-10, 10)
ax.set_ylim(0, 0.1)
ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$|\psi(x)|^2$')

line = None
title = None

def init():
    global line, title
    line, = ax.plot([], [], animated=True)
    title = ax.set_title('$t = 00.00$')
    title.set_ha('left')
    return line, title

def animate(i):
    global line, title
    line.set_data(x, psi[i])
    title.set_text('$t = {:05.2f}$'.format(t[i]))
    print('{} / {}'.format(i, len(t)))
    return line, title

anim = mpl.animation.FuncAnimation(fig, animate, init_func=init, frames=len(t), blit=True)
anim.save('{}.mp4'.format(sys.argv[1].rsplit('.', 1)[0]), fps=100, extra_args=['-vcodec', 'libx264'])
