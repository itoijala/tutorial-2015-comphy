#include <cmath>
#include <cstdio>
#include <complex>
#include <functional>
#include <string>
#include <tuple>

#include <Eigen/Dense>

using std::complex;
using std::function;
using std::make_tuple;
using std::string;
using std::tie;
using std::tuple;

using Eigen::MatrixXcd;
using Eigen::VectorXcd;
using Eigen::VectorXd;

typedef complex<double> C;

typedef function<tuple<VectorXd, MatrixXcd>(int, double, double, double)> Hamiltonian;
typedef function<MatrixXcd(const MatrixXcd&, double)> TimeEvolution;

MatrixXcd ftcs(const MatrixXcd& H, const double dt)
{
	const auto i = C(0., 1.);
	const auto I = MatrixXcd::Identity(H.rows(), H.cols());

	return I - i * dt * H;
}

MatrixXcd crank_nicholson(const MatrixXcd& H, const double dt)
{
	#pragma GCC diagnostic push
	#pragma GCC diagnostic ignored "-Wconversion"

	const auto i = C(0., 1.);
	const auto I = MatrixXcd::Identity(H.rows(), H.cols());

	return   (I + 0.5 * i * dt * H).inverse()
	       * (I - 0.5 * i * dt * H);

	#pragma GCC diagnostic pop
}

tuple<VectorXd, MatrixXcd> H_harmonic(const int N, const double dx, const double x1, const double x2)
{
	const VectorXd x = VectorXd::LinSpaced(Eigen::Sequential, N, x1, x2);

	MatrixXcd H = 2. / pow(dx, 2) * MatrixXcd::Identity(N, N);
	H.topRightCorner(N - 1, N - 1) -= 1. / pow(dx, 2) * MatrixXcd::Identity(N - 1, N - 1);
	H.bottomLeftCorner(N - 1, N - 1) -= 1. / pow(dx, 2) * MatrixXcd::Identity(N - 1, N - 1);
	H += x.unaryExpr([] (double x) { return pow(x, 2); }).cast<C>().asDiagonal();

	return make_tuple(x, H);
}

tuple<VectorXd, MatrixXcd> H_anharmonic(const int N, const double dx, const double x1, const double x2)
{
	const double epsilon = 0.1;

	VectorXd x;
	MatrixXcd H;
	tie(x, H) = H_harmonic(N, dx, x1, x2);

	H += epsilon * x.unaryExpr([] (double x) { return pow(x, 4); }).cast<C>().asDiagonal();

	return make_tuple(x, H);
}

tuple<double, double> mean_variance(const MatrixXcd& O, const VectorXcd& psi)
{
	const auto phi = O * psi;
	const double mean = psi.dot(phi).real();
	const double variance = phi.dot(phi).real() - pow(mean, 2);
	return make_tuple(mean, variance);
}

MatrixXcd p_operator(const int N, const double dx)
{
	MatrixXcd p = MatrixXcd::Zero(N, N);
	p.topRightCorner(N - 1, N - 1) -= MatrixXcd::Identity(N - 1, N - 1);
	p.bottomLeftCorner(N - 1, N - 1) += MatrixXcd::Identity(N - 1, N - 1);
	p /= -2. * C(0, 1) * dx;
	return p;
}

void print(FILE* file, const VectorXd& v)
{
	for (int i = 0; i < v.size(); ++i)
	{
		fprintf(file, " %.20e", v[i]);
	}
}

void work(const string& name,
        const Hamiltonian& hamiltonian,
        const TimeEvolution& time_evolution,
        const size_t T)
{
	const double dt = 0.01;
	const double dx = 0.1;
	const int N = 10 * 20 + 1;
	const double x1 = -10.;
	const double x2 = +10.;

	const double mu = 1.;
	const double sigma = 1.;

	VectorXd x;
	MatrixXcd H;
	tie(x, H) = hamiltonian(N, dx, x1, x2);

	MatrixXcd S = time_evolution(H, dt);

	MatrixXcd O_x = x.cast<C>().asDiagonal();
	MatrixXcd O_p = p_operator(N, dx);

	VectorXcd psi = x.unaryExpr([mu, sigma] (double x)
	{
		return exp(-0.25 / sigma * pow(x - mu, 2));
	}).cast<C>().normalized();

	auto file_psi = fopen(("1-" + name + "-psi.dat").c_str(), "w");
	auto file_meas = fopen(("1-" + name + "-meas.dat").c_str(), "w");

	fprintf(file_psi, "# t psi(x)...\n");
	fprintf(file_meas, "# t |psi|² <x> <x²>-<x>² <p> <p²>-<p>²\n");

	for (size_t i = 0; i <= T; ++i, psi = S * psi)
	{
		const double t = double(i) * dt;

		fprintf(file_psi, "%.20e ", t);
		print(file_psi, psi.cwiseAbs2());
		fprintf(file_psi, "\n");

		const double norm2 = psi.squaredNorm();

		double mx, vx, mp, vp;
		tie(mx, vx) = mean_variance(O_x, psi);
		tie(mp, vp) = mean_variance(O_p, psi);

		fprintf(file_meas, "%.20e %.20e %.20e %.20e %.20e %.20e\n", t, norm2, mx, vx, mp, vp);
	}

	fclose(file_psi);
	fclose(file_meas);
}

int usage(char* argv0)
{
	fprintf(stderr, "Usage: %s harmonic CN|FTCS\n", argv0);
	return 1;
}

int main(int argc, char** argv)
{
	if (argc == 3)
	{
		string a_H = argv[1];
		Hamiltonian H;
		if (a_H == "harmonic")
		{
			H = H_harmonic;
		}
		else if (a_H == "anharmonic")
		{
			H = H_anharmonic;
		}
		else
		{
			return usage(argv[0]);
		}

		string a_S = argv[2];
		TimeEvolution S;
		size_t T;
		if (a_S == "CN")
		{
			S = crank_nicholson;
			T = 100 * 50;
		}
		else if (a_S == "FTCS")
		{
			S = ftcs;
			T = 17;
		}
		else
		{
			return usage(argv[0]);
		}

		work(a_H + "-" + a_S, H, S, T);
	}
	else
	{
		return usage(argv[0]);
	}
}
