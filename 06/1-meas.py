#!/usr/bin/env python3

import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

t, norm2, mx, vx, mp, vp = np.loadtxt(sys.argv[1], unpack=True, skiprows=1)

fig, (ax1, ax2) = plt.subplots(2, 1)
fig.set_figheight(2 * fig.get_figheight())

ax1.plot(t, norm2 - 1)

ax1.set_xlabel(r'$t$')
ax1.set_ylabel(r'$|\psi|^2 - 1$')

ax2.plot(t, mx, label=r'$\langle x \rangle$')
ax2.plot(t, vx, label=r'$\langle x^2 \rangle - \langle x \rangle^2$')

ax2.plot(t, mp, label=r'$\langle p \rangle$')
ax2.plot(t, vp, label=r'$\langle p^2 \rangle - \langle p \rangle^2$')
ax2.plot(t, np.sqrt(vx * vp), label=r'$\Delta x \, \Delta p$')

ax2.set_xlabel(r'$t$')
ax2.legend(loc="upper center", ncol=3, mode='expand')

fig.savefig('{}.pdf'.format(sys.argv[1].rsplit('.', 1)[0]))
