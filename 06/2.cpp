#include <cmath>
#include <cstdio>
#include <functional>
#include <string>
#include <tuple>

#include <Eigen/Core>

using std::function;
using std::make_tuple;
using std::string;
using std::tie;
using std::tuple;

using Eigen::ArrayXd;

using ArrayXc = Eigen::Array<unsigned char, Eigen::Dynamic, 1>;

using Index = ArrayXd::Index;

void printArray(const string& name, const ArrayXd& a, const Index Nx)
{
	auto file = fopen(name.c_str(), "w");

	for (Index y = 0; y < a.size() / Nx; ++y)
	{
		for (Index x = 0; x < Nx; ++x)
		{
			fprintf(file, " %.20e", a[y * Nx + x]);
		}
		fprintf(file, "\n");
	}

	fclose(file);
}

struct JacobiSimple
{
	template <typename T>
	static T to(const T& a, const Index Nx)
	{
		return a;
	}

	template <typename T>
	static T from(const T& a, const Index Nx)
	{
		return a;
	}

	static double iteration(ArrayXd& phi0, const ArrayXd& rho, const ArrayXc& mask, const double dx, const Index Nx)
	{
		ArrayXd phi(phi0.size());

		double sum = 0.;

		for (Index i = 0; i < phi.size(); ++i)
		{
			if (mask[i] == 0)
			{
				phi[i] = 0.25 * (phi0[i - Nx]
				               + phi0[i - 1]
				               + phi0[i + 1]
				               + phi0[i + Nx]
				               + pow(dx, 2) * rho[i]);

				sum += fabs(phi[i] - phi0[i]);
			}
			else
			{
				phi[i] = phi0[i];
			}
		}

		phi0 = phi;

		return sum;
	}
};

struct JacobiParallel
{
	template <typename T>
	static T to(const T& a, const Index Nx)
	{
		return a;
	}

	template <typename T>
	static T from(const T& a, const Index Nx)
	{
		return a;
	}

	static double iteration(ArrayXd& phi0, const ArrayXd& rho, const ArrayXc& mask, const double dx, const Index Nx)
	{
		ArrayXd phi(phi0.size());

		double sum = 0.;

		#pragma omp parallel for reduction(+:sum)
		for (Index i = 0; i < phi.size(); ++i)
		{
			if (mask[i] == 0)
			{
				phi[i] = 0.25 * (phi0[i - Nx]
				               + phi0[i - 1]
				               + phi0[i + 1]
				               + phi0[i + Nx]
				               + pow(dx, 2) * rho[i]);

				sum += fabs(phi[i] - phi0[i]);
			}
			else
			{
				phi[i] = phi0[i];
			}
		}

		phi0 = phi;

		return sum;
	}
};

struct GaussSeidelSimple
{
	template <typename T>
	static T to(const T& a, const Index Nx)
	{
		return a;
	}

	template <typename T>
	static T from(const T& a, const Index Nx)
	{
		return a;
	}

	static double iteration(ArrayXd& phi, const ArrayXd& rho, const ArrayXc& mask, const double dx, const Index Nx)
	{
		double sum = 0.;

		for (Index i = 0; i < phi.size(); ++i)
		{
			if (mask[i] == 0)
			{
				double tmp = 0.25 * (phi[i - Nx]
				                   + phi[i - 1]
				                   + phi[i + 1]
				                   + phi[i + Nx]
				                   + pow(dx, 2) * rho[i]);

				sum += fabs(tmp - phi[i]);

				phi[i] = tmp;
			}
		}

		return sum;
	}
};

struct GaussSeidelParallel
{
	template <typename T>
	static T to(const T& a, const Index Nx)
	{
		return a;
	}

	template <typename T>
	static T from(const T& a, const Index Nx)
	{
		return a;
	}

	static double iteration(ArrayXd& phi, const ArrayXd& rho, const ArrayXc& mask, const double dx, const Index Nx)
	{
		const Index N = phi.size();

		double sum = 0.;

		#pragma omp parallel for reduction(+:sum)
		for (Index y = 0; y < Nx; ++y)
		{
			for (Index x = y % 2; x < N / Nx; x += 2)
			{
				const Index i = y * Nx + x;

				if (mask[i] == 0)
				{
					double tmp = 0.25 * (phi[i - Nx]
					                   + phi[i - 1]
					                   + phi[i + 1]
					                   + phi[i + Nx]
					                   + pow(dx, 2) * rho[i]);

					sum += fabs(tmp - phi[i]);

					phi[i] = tmp;
				}
			}
		}

		#pragma omp parallel for reduction(+:sum)
		for (Index y = 0; y < Nx; ++y)
		{
			for (Index x = 1 - y % 2; x < N / Nx; x += 2)
			{
				const Index i = y * Nx + x;

				if (mask[i] == 0)
				{
					double tmp = 0.25 * (phi[i - Nx]
					                   + phi[i - 1]
					                   + phi[i + 1]
					                   + phi[i + Nx]
					                   + pow(dx, 2) * rho[i]);

					sum += fabs(tmp - phi[i]);

					phi[i] = tmp;
				}
			}
		}

		return sum;
	}
};

struct GaussSeidelClever
{
	template <typename T>
	static T to(const T& canon, const Index Nx)
	{
		const Index N = canon.size();
		const Index n = N / 2;
		const Index nx = Nx / 2;

		T clever(N);

		for (Index i = 0; i < n; ++i)
		{
			clever[i]     = canon[2 * i + ((i / nx) % 2)];
			clever[n + i] = canon[2 * i - ((i / nx) % 2) + 1];
		}

		return clever;
	}

	template <typename T>
	static T from(const T& clever, const Index Nx)
	{
		const Index N = clever.size();
		const Index n = N / 2;
		const Index nx = Nx / 2;

		T canon(N);

		for (Index i = 0; i < n; ++i)
		{
			canon[2 * i + ((i / nx) % 2)]     = clever[i];
			canon[2 * i - ((i / nx) % 2) + 1] = clever[n + i];
		}

		return canon;
	}

	static double iteration(ArrayXd& phi, const ArrayXd& rho, const ArrayXc& mask, const double dx, const Index Nx)
	{
		const Index n = phi.size() / 2;
		const Index nx = Nx / 2;

		double sum = 0.;

		#pragma omp parallel for reduction(+:sum)
		for (Index y = 0; y < n / nx; ++y)
		{
			for (Index x = 0; x < nx; ++x)
			{
				const Index i = y * nx + x;

				if (mask[i] == 0)
				{
					double tmp = 0.25 * (phi[n + i - nx]
					                   + phi[n + i]
					                   + phi[n + i - 1 + 2 * (y % 2)]
					                   + phi[n + i + nx]
					                   + pow(dx, 2) * rho[i]);

					sum += fabs(tmp - phi[i]);

					phi[i] = tmp;
				}
			}
		}

		#pragma omp parallel for reduction(+:sum)
		for (Index y = 0; y < n / nx; ++y)
		{
			for (Index x = 0; x < nx; ++x)
			{
				const Index i = y * nx + x;

				if (mask[n + i] == 0)
				{
					double tmp = 0.25 * (phi[i - nx]
					                   + phi[i]
					                   + phi[i + 1 - 2 * (y % 2)]
					                   + phi[i + nx]
					                   + pow(dx, 2) * rho[n + i]);

					sum += fabs(tmp - phi[n + i]);

					phi[n + i] = tmp;
				}
			}
		}

		return sum;
	}
};

template <typename Alg>
ArrayXd iterate(const ArrayXd& phi0, const ArrayXd& rho0, const ArrayXc& mask0,
        const double dx, const Index Nx)
{
	const double epsilon = 1e-5;

	ArrayXd phi = Alg::template to<ArrayXd>(phi0, Nx);
	const ArrayXd rho = Alg::template to<ArrayXd>(rho0, Nx);
	const ArrayXc mask = Alg::template to<ArrayXc>(mask0, Nx);

	size_t c = 0;
	while (Alg::iteration(phi, rho, mask, dx, Nx) > epsilon)
	{
		++c;
	}
	fprintf(stderr, "  %zu iterations\n", c);

	phi = Alg::template from<ArrayXd>(phi, Nx);

	return phi;
}

template <typename Alg>
tuple<ArrayXd, Index> grow(const function<void(ArrayXd&, ArrayXd&, ArrayXc&, Index, double)>& start, const Index N_max)
{
	Index N = 16;
	ArrayXd phi = ArrayXd::Zero(N * N);

	do
	{
		N *= 2;

		fprintf(stderr, "N = %ld\n", N);

		ArrayXd big(N * N);
		for (Index y = 0; y < N; ++y)
		{
			for (Index x = 0; x < N; ++x)
			{
				big[y * N + x] = phi[y/2 * N/2 + x/2];
			}
		}
		phi = big;

		ArrayXd rho = ArrayXd::Zero(N * N);
		ArrayXc mask = ArrayXc::Zero(N * N);
		start(phi, rho, mask, N, 1. / double(N));

		phi = iterate<Alg>(phi, rho, mask, 1. / double(N), N);
	}
	while (N < N_max);

	return make_tuple(phi, N);
}

template <typename Alg>
void work(const string& name, const function<void(ArrayXd&, ArrayXd&, ArrayXc&, Index, double)>& start, const Index N_max)
{
	ArrayXd phi;
	Index N;
	tie(phi, N) = grow<Alg>(start, N_max);

	printArray(name, phi, N);
}

void draw_box(ArrayXd& phi, ArrayXc& mask,
        const Index Nx, const Index Ny,
        const double top, const double left, const double right, const double bottom)
{
	for (Index x = 0; x < Nx; ++x)
	{
		phi[x] = top;
		phi[(Ny - 1) * Nx + x] = bottom;
		mask[x] = 1;
		mask[(Ny - 1) * Nx + x] = 1;
	}

	for (Index y = 0; y < Ny; ++y)
	{
		phi[y * Nx] = left;
		phi[y * Nx + Nx - 1] = right;
		mask[y * Nx] = 1;
		mask[y * Nx + Nx - 1] = 1;
	}
}

void start_a(ArrayXd& phi, ArrayXd& rho, ArrayXc& mask, const Index N, const double dx)
{
	draw_box(phi, mask, N, N, 0., 0., 0., 0.);
}

void start_b(ArrayXd& phi, ArrayXd& rho, ArrayXc& mask, const Index N, const double dx)
{
	draw_box(phi, mask, N, N, 0., 0., 0., 1.);
}

void start_c(ArrayXd& phi, ArrayXd& rho, ArrayXc& mask, const Index N, const double dx)
{
	draw_box(phi, mask, N, N, 0., 0., 0., 0.);

	rho[N/3 * N + N/3] = 1. / pow(dx, 2);
}

void start_e(ArrayXd& phi, ArrayXd& rho, ArrayXc& mask, const Index N, const double dx)
{
	draw_box(phi, mask, N, N, 0., 0., 0., 0.);

	rho[N/3 * N + N/3] = 1. / pow(dx, 2);
	rho[2 * N/3 * N + N/3] = -2. / pow(dx, 2);
	rho[N/2 * N + 2 * N/3] = 1. / pow(dx, 2);
}

int usage(char* argv0)
{
	fprintf(stderr, "Usage: %s a|b|c|e JS|JP|GSS|GSP|GSC\n", argv0);
	return 1;
}

int main(int argc, char** argv)
{
	if (argc == 3)
	{
		function<void(ArrayXd&, ArrayXd&, ArrayXc&, Index, double)> start;

		string a_ex = argv[1];
		if (a_ex == "a")
		{
			start = start_a;
		}
		else if (a_ex == "b")
		{
			start = start_b;
		}
		else if (a_ex == "c")
		{
			start = start_c;
		}
		else if (a_ex == "e")
		{
			start = start_e;
		}
		else
		{
			return usage(argv[0]);
		}

		function<void(const string&, const function<void(ArrayXd&, ArrayXd&, ArrayXc&, Index, double)>&, Index)> alg;

		string a_alg = argv[2];
		if (a_alg == "JS")
		{
			alg = work<JacobiSimple>;
		}
		else if (a_alg == "JP")
		{
			alg = work<JacobiParallel>;
		}
		else if (a_alg == "GSS")
		{
			alg = work<GaussSeidelSimple>;
		}
		else if (a_alg == "GSP")
		{
			alg = work<GaussSeidelParallel>;
		}
		else if (a_alg == "GSC")
		{
			alg = work<GaussSeidelClever>;
		}
		else
		{
			return usage(argv[0]);
		}

		alg("2-" + a_ex + "-" + a_alg + ".dat", start, 512);
	}
	else
	{
		return usage(argv[0]);
	}
}
