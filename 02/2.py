#!/usr/bin/env python3

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

def plot(name, x, phi, asymptotic, ylim, label):
    fig, ax = plt.subplots(1, 1)

    ax.plot(x, phi, label='numeric')
    ax.plot(x, asymptotic(x), label=label)

    ax.set_ylim(*ylim)
    ax.set_xlabel(r'$x$')
    ax.set_ylabel(r'$\phi(x)$')
    ax.legend(loc='best')
    fig.savefig('{}.pdf'.format(name))

x, a, c = np.loadtxt('2.dat', unpack=True)

plot('2a', x, a, lambda x: 8 / np.abs(x), (0, 10), r'$\displaystyle 8 \frac{1}{|x|}$')
plot('2c', x, c, lambda x: 8/3 * x / np.abs(x)**3, (-2, 2), r'$\displaystyle \frac{8}{3} \frac{x}{|x|^3}$')
