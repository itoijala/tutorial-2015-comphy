#include <cmath>
#include <cstdio>

#include "integrate.hpp"

double iterate(const Integrator& s, const Integrand& f, const double a, const double b, const double epsilon)
{
	size_t N = 2;
	double old;
	double current = 0.;

	do
	{
		old = current;
		current = s(f, a, b, N);

		N *= 2;
	}
	while (std::abs((old - current) / old) >= epsilon);

	return current;
}

double iterate_infinite(const Integrator& s, const Integrand& f, const double a, const double epsilon)
{
	double b = 2. * a + 1.;
	size_t N = 2;
	double old;
	double current = 0.;

	do
	{
		old = current;
		current = s(f, a, b, N);

		b *= 2.;
		N *= 4;
	}
	while (std::abs((old - current) / old) >= epsilon);

	return current;
}

void a()
{
	const double epsilon = 1e-5;
	const double Delta = 1e-6;
	const double a = -1.;
	const double b = 1.;
	const double z = 0.;

	const auto f1 = [] (double t) { return exp(t) / t; };
	const auto f2 = [Delta] (double s)
	{
		if (s < 1e-10)
		{
			return 0.;
		}
		return (exp(Delta * s) - 1.) / s;
	};

	const double I1 = iterate(simpson, f1, a, z - Delta, 0.1 * epsilon);
	const double I2 = iterate(simpson, f2, -1., 1., 0.1 * epsilon);
	const double I3 = iterate(simpson, f1, z + Delta, b, 0.1 * epsilon);

	printf("I1: %.7f\n", I1);
	printf("I2: %.7f\n", I2);
	printf("I3: %.7f\n", I3);

	printf("\n");
	printf("I_1:     %.7f\n", I1 + I2 + I3);
	printf("correct: %.7f\n", 2.1145018);
}

void b()
{
	const double epsilon = 1e-5;
	const double a = 0.;

	const auto f = [] (double t) { return 2. * exp(- t * t); };

	const double I = iterate_infinite(simpson, f, a, epsilon);

	printf("I_2:     %.7f\n", I);
	printf("correct: %.7f\n", 1.7724538);
}

int main()
{
	a();
	printf("\n");
	b();
}
