#include <cstdio>
#include <functional>

#include <Eigen/Core>

#include "integrate.hpp"

using std::function;

using Eigen::VectorXd;

// potential ϕ in units of ρ₀ a² / (4 π ε₀)

template<int N>
using Vector = Eigen::Matrix<double, N, 1>;

template<int N, class F>
double integrate(const F& f, const Vector<N>& a, const Vector<N>& b, const int i, Vector<N>& x)
{
	if (i == N)
	{
		return f(x);
	}

	return simpson([&f, &x, &a, &b, i] (const double y)
	{
		x[i] = y;
		return integrate(f, a, b, i + 1, x);
	}, a[i], b[i], 100);
}

template<int N, class F>
double integrate(const F& f, const Vector<N>& a, const Vector<N>& b)
{
	Vector<N> x;
	return integrate(f, a, b, 0, x);
}

template<int N>
function<double(const Vector<N>&)> potential(const function<double(const Vector<N>&)>& rho)
{
	return [rho] (const Vector<N>& x)
	{
		const auto integrand = [&rho, &x] (const Vector<N>& y)
		{
			const double n = (x - y).norm();
			if (n < 2. / 100. / 2.)
			{
				return 0.;
			}
			return rho(y) / (x - y).norm();
		};
		const Vector<N> a = - Vector<N>::Ones();
		const Vector<N> b = Vector<N>::Ones();
		return integrate(integrand, a, b);
	};
}

int main()
{
	const auto rho_a = [] (const Vector<3>& x) { return 1.; };
	const auto rho_c = [] (const Vector<3>& x) { return x[0]; };
	const auto phi_a = potential<3>(rho_a);
	const auto phi_c = potential<3>(rho_c);

	auto file = fopen("2.dat", "w");
	fprintf(file, "# x phi_a phi_c\n");

	for (int n = -100; n <= 100; ++n)
	{
		const Vector<3> x(0.1 * n, 0, 0);
		fprintf(file, "%.20e %.20e %.20e\n", x[0], phi_a(x), phi_c(x));
	}

	fclose(file);
}
