#include <functional>

typedef std::function<double(double)> Integrand;

typedef std::function<double(const Integrand&, double, double, size_t)> Integrator;

double simpson(const Integrand& f, const double a, const double b, const size_t N)
{
	const double h = (b - a) / N;
	double sum = 0.;

	for (size_t i = 2; i < N; i += 2)
	{
		sum += 2. * f(a + h * i);
	}

	for (size_t i = 1; i < N; i += 2)
	{
		sum += 4. * f(a + h * i);
	}

	sum += f(a) + f(b);
	sum *= h / 3.;
	return sum;
}
