#include <cctype>
#include <cmath>
#include <cstdio>
#include <functional>
#include <stdexcept>
#include <string>

#include <Eigen/Dense>

using std::function;
using std::string;

using Eigen::MatrixXd;
using Eigen::VectorXd;

using I = MatrixXd::Index;

MatrixXd H_position(const I N, const double lambda)
{
	const VectorXd x = VectorXd::LinSpaced(N, -10., 10.);

	const double f = 1. / pow(20. / double(N), 2);

	MatrixXd H = 2. * f * MatrixXd::Identity(N, N);
	H.diagonal(1) -= f * VectorXd::Constant(N - 1, 1.);
	H.diagonal(-1) -= f * VectorXd::Constant(N - 1, 1.);

	H.diagonal() += x.unaryExpr([] (double x) { return pow(x, 2); });

	H.diagonal() += lambda * x.unaryExpr([] (double x) { return pow(x, 4); });

	return H;
}

MatrixXd H_occupation(const I N, const double lambda)
{
	MatrixXd H = MatrixXd::Identity(N, N);

	H.diagonal() += VectorXd::LinSpaced(N, 0., double(N - 1)).unaryExpr(
		[] (double m) { return 2. * m; }
	);

	H.diagonal() += 0.25 * lambda * VectorXd::LinSpaced(N, 0., double(N - 1)).unaryExpr(
		[] (double m) { return 6. * pow(m, 2) + 6. * m + 3; }
	);

	H.diagonal(-2) += 0.25 * lambda * VectorXd::LinSpaced(N - 2, 0., double(N - 3)).unaryExpr(
		[] (double m) { return sqrt((m + 1) * (m + 2)) * (4 * m + 6); }
	);

	H.diagonal(+2) += 0.25 * lambda * VectorXd::LinSpaced(N - 2, 2., double(N - 1)).unaryExpr(
		[] (double m) { return sqrt(m * (m - 1)) * (4 * m - 2); }
	);

	H.diagonal(-4) += 0.25 * lambda * VectorXd::LinSpaced(N - 4, 0., double(N - 5)).unaryExpr(
		[] (double m) { return sqrt((m + 1) * (m + 2) * (m + 3) * (m + 4)); }
	);

	H.diagonal(+4) += 0.25 * lambda * VectorXd::LinSpaced(N - 4, 4., double(N - 1)).unaryExpr(
		[] (double m) { return sqrt(m * (m - 1) * (m - 2) * (m - 3)); }
	);

	return H;
}

void work(const function<MatrixXd(I, double)>& f_H, const I N, const double lambda)
{
	const MatrixXd H = f_H(N, lambda);

	VectorXd E = Eigen::SelfAdjointEigenSolver<MatrixXd>(H, Eigen::EigenvaluesOnly).eigenvalues();

	for (I i = 0; i < E.size(); ++i)
	{
		printf("%.20e\n", E[i]);
	}
}

double string_to_double(const string& s)
{
	size_t i = 0;
	double r = std::stod(s, &i);
	if (i != s.length() || isspace(s[0]))
	{
		throw std::invalid_argument(__FUNCTION__ + (": '" + s + "'"));
	}
	return r;
}

I string_to_index(const string& s)
{
	size_t i = 0;
	I r = std::stoll(s, &i);
	if (i != s.length() || isspace(s[0]))
	{
		throw std::invalid_argument(__FUNCTION__ + (": '" + s + "'"));
	}
	return r;
}

int usage(char** argv)
{
	fprintf(stderr, "Usage: %s position|occupation <N> <λ>\n", argv[0]);
	return 1;
}

int main(int argc, char** argv)
{
	if (argc != 4)
	{
		return usage(argv);
	}

	const I N = string_to_index(argv[2]);
	const double lambda = string_to_double(argv[3]);

	string a(argv[1]);
	if (a == "position")
	{
		work(H_position, N, lambda);
	}
	else if (a == "occupation")
	{
		work(H_occupation, N, lambda);
	}
	else
	{
		return usage(argv);
	}
}
