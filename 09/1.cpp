#include <cmath>
#include <cstdio>
#include <limits>
#include <string>

#include <Eigen/Dense>

using std::string;

using Eigen::MatrixXd;
using Eigen::VectorXd;

using I = VectorXd::Index;

// http://stackoverflow.com/a/4609795
template <typename T>
int sgn(T val)
{
	return (T(0) < val) - (val < T(0));
}

VectorXd eigenSolver(const MatrixXd& A)
{
	Eigen::SelfAdjointEigenSolver<MatrixXd> s(A, Eigen::EigenvaluesOnly);
	return s.eigenvalues();
}

VectorXd powerSolver(const MatrixXd& A)
{
	const double epsilon = 1e-7;
	const I N = A.rows();

	MatrixXd B = A;

	VectorXd lambda(N);
	MatrixXd v(N, N);

	MatrixXd X = MatrixXd::Identity(N, N);

	for (I i = 0; i < N; ++i)
	{
		// Gram–Schmidt
		if (i > 0)
		{
			X -= v.col(i - 1) * (X.transpose() * v.col(i - 1)).transpose();
		}

		I j;
		X.colwise().norm().maxCoeff(&j);
		VectorXd x_new = X.col(j).normalized();
		VectorXd x_old;

		double l_old;
		double l_new = std::numeric_limits<double>::infinity();

		do
		{
			x_old = x_new;
			x_new = (B * x_old);

			l_old = l_new;
			l_new = x_old.dot(x_new);
			
			x_new.normalize();
		}
		while (fabs(l_new - l_old) >= epsilon * fabs(l_new + l_old));

		B -= l_new * x_new * x_new.transpose();

		lambda[i] = l_new;
		v.col(i) = x_new;
	}

	return lambda;
}

VectorXd jacobiSolver(const MatrixXd& A)
{
	const double epsilon = 1e-7;
	const I N = A.rows();

	MatrixXd B = A;

	double off = B.cwiseAbs2().sum() - B.diagonal().squaredNorm();

	while (off > epsilon)
	{
		for (I q = 0; q < N - 1; ++q)
		{
			for (I p = q + 1; p < N; ++p)
			{
				double old = B(p, q);

				// Works for old == 0!
				double Theta = (B(q, q) - B(p, p)) / (2. * old);
				double t = sgn(Theta) / (fabs(Theta) + sqrt(pow(Theta, 2) + 1.));
				double c = 1. / sqrt(1. + pow(t, 2));
				double s = t * c;

				MatrixXd P = MatrixXd::Identity(N, N);
				P(p, p) = c;
				P(p, q) = s;
				P(q, p) = -s;
				P(q, q) = c;

				B = P.transpose() * B * P;

				off -= 2. * pow(old, 2);
			}
		}
	}

	return B.diagonal();
}

template <class Solver>
void work(const Solver& solver)
{
	MatrixXd A(4, 4);
	A << 1, 2, 2, 4,
	     2, 5, 2, 1,
	     2, 2, 6, 3,
	     4, 1, 3, 4;

	VectorXd lambda = solver(A);
	std::sort(lambda.data(), lambda.data() + lambda.size(), [] (double x, double y) { return x > y; });

	for (I i = 0; i < lambda.size(); ++i)
	{
		printf("%.20e\n", lambda[i]);
	}
}

int usage(char** argv)
{
	fprintf(stderr, "Usage: %s eigen|power|jacobi\n", argv[0]);
	return 1;
}

int main(int argc, char** argv)
{
	if (argc == 2)
	{
		string a(argv[1]);
		if (a == "eigen")
		{
			work(eigenSolver);
		}
		else if (a == "power")
		{
			work(powerSolver);
		}
		else if (a == "jacobi")
		{
			work(jacobiSolver);
		}
		else
		{
			return usage(argv);
		}
	}
	else
	{
		return usage(argv);
	}
}
