#include <cmath>
#include <cstdio>
#include <functional>
#include <string>
#include <vector>

#include <Eigen/Core>

#include "rungeKutta.hpp"
#include "utils.hpp"
#include "zeeman.hpp"

using std::function;
using std::string;

using Eigen::Vector2d;

function<double(double, double)> make_V(const double a, const double b, const double R, const double k, const function<Vector2d(double)>& xy)
{
	return [a, b, R, k, xy] (const double t, const double theta)
	{
		const Vector2d r = R * Vector2d(cos(theta), sin(theta));

		const double d1 = (Vector2d(-a, 0.) - r).norm();
		const double d2 = (xy(t) - r).norm();

		return 0.5 * k * (pow(d1 - b, 2) + pow(d2 - b, 2));
	};
}

function<double(double)> derive(double h, const function<double(double)> f)
{
	return [f, h] (double x)
	{
		return (f(x + h) - f(x - h)) / (2. * h);
	};
}

void work_a(const string& name, const double a, const double b, const double R, const double k, const double x, const double y)
{
	const auto xy = [x, y] (const double t) { return Vector2d(x, y); };
	const auto V = make_V(a, b, R, k, xy);
	const auto F_ana = make_F(a, b, R, k, xy);
	const auto F_num = derive(1e-3, [V] (const double theta) { return - V(0., theta); });

	auto file = fopen(name.c_str(), "w");
	fprintf(file, "# θ V(θ) F_ana(θ) F_num(θ)\n");

	for (size_t i = 0; i <= 1000; ++i)
	{
		const double theta = 2 * M_PI / double(1000) * double(i);
		fprintf(file, "%.20e %.20e %.20e %.20e\n", theta, V(0., theta), F_ana(0., theta), F_num(theta));
	}

	fclose(file);
}

void work_b(const string& name, const double a, const double b, const double R, const double k,
        const double I, const double gamma,
        const double x, const double y,
        const double theta0, const double omega0)
{
	const double h = 1e-3;
	const double T = 50.;

	const auto xy = [x, y] (const double t) { return Vector2d(x, y); };
	const auto de = make_de(a, b, R, k, I, gamma, xy);

	auto file = fopen(name.c_str(), "w");
	fprintf(file, "# t θ ω\n");

	solve_write(file, de, h, 0., T, Vector2d(theta0, omega0));

	fclose(file);
}

void work_c(const double a, const double b, const double R, const double k,
        const double I, const double gamma)
{
	const Vector2d xy0(16., 0.);
	const Vector2d xy1(16., 2.);

	const double h = 1e-3;
	const double T = 100.;

	#pragma GCC diagnostic push
	#pragma GCC diagnostic ignored "-Wpadded"

	const auto de0 = make_de(a, b, R, k, I, gamma,
	        [xy0, xy1, T] (const double t) { return xy0 + t / T * (xy1 - xy0); });

	auto file = fopen("1c.dat", "w");
	fprintf(file, "# t θ ω\n");

	Vector2d to1 = solve_write(file, de0, h, 0., T, Vector2d(3.88, 0.));
	printf("θ(T) = %f\n", to1[0]);

	const auto de1 = make_de(a, b, R, k, I, gamma,
	        [xy0, xy1, T] (const double t) { return xy1 + (t - T) / T * (xy0 - xy1); });

	#pragma GCC diagnostic pop

	solve_write(file, de1, h, T, 2. * T, Vector2d(to1[0], 0.));
}

int main(int argc, char** argv)
{
	const double a = 20.;
	const double b = 10.;
	const double R = 5.;
	const double k = 250.;
	const double I = 6250.;
	const double gamma = 2000.;

	if (argc == 3)
	{
		const double x = string_to_double(argv[1]);
		const double y = string_to_double(argv[2]);

		char name[256];
		snprintf(name, 256, "1a_%.0f_%.0f.dat", x, y);

		work_a(name, a, b, R, k, x, y);
	}
	else if (argc == 5)
	{
		const double x = string_to_double(argv[1]);
		const double y = string_to_double(argv[2]);
		const double theta0 = string_to_double(argv[3]);
		const double omega0 = string_to_double(argv[4]);

		char name[256];
		snprintf(name, 256, "1b_%.0f_%.0f_%.0f_%.0f.dat", x, y, theta0, omega0);

		work_b(name, a, b, R, k, I, gamma, x, y, theta0, omega0);
	}
	else if (argc == 1)
	{
		work_c(a, b, R, k, I, gamma);
	}
	else
	{
		fprintf(stderr, "Usage: %s [<x> <y> [<θ0> <ω0>]]\n", argv[0]);
		return 1;
	}
}
