#!/usr/bin/env python3

import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

t, θ, ω = np.loadtxt(sys.argv[1], unpack=True, skiprows=1)

fig, ax = plt.subplots(1, 1)

ax.plot(t, θ, label=r'$\theta(t)$')
ax.plot(t, ω, label=r'$\omega(t)$')

ax.grid()
ax.legend(loc='best')
ax.set_xlabel(r'$t$')

fig.savefig('{}.pdf'.format(sys.argv[1].rsplit('.', 1)[0]))
