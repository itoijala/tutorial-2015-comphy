#!/usr/bin/env python3

import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

t, θ, ω = np.loadtxt(sys.argv[1], unpack=True, skiprows=1)

fig, ax = plt.subplots(1, 1)

ax.plot(θ, ω, ',')

ax.grid()
ax.set_xlabel(r'$\theta$')
ax.set_ylabel(r'$\omega$')

fig.savefig('{}.pdf'.format(sys.argv[1].rsplit('.', 1)[0]))
