#!/usr/bin/env python3

import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

θ, V, F_ana, F_num = np.loadtxt(sys.argv[1], unpack=True, skiprows=1)

fig, ax = plt.subplots(1, 1)

ax.plot(θ, V, label=r'$V$')
ax.plot(θ, F_ana, label=r'$F_\text{ana}$')
ax.plot(θ, F_num, '--', label=r'$F_\text{num}$')

ax.grid()
ax.legend(loc='best')
ax.set_xlabel(r'$\theta$')

fig.savefig('{}.pdf'.format(sys.argv[1].rsplit('.', 1)[0]))
