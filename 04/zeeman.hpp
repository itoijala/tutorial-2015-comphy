#include <cmath>
#include <functional>

#include <Eigen/Core>

#include "rungeKutta.hpp"

std::function<double(double, double)> make_F(const double a, const double b, const double R, const double k, const std::function<Eigen::Vector2d(double)>& xy)
{
	return [a, b, R, k, xy] (const double t, const double theta)
	{
		const Eigen::Vector2d r(R * cos(theta), R * sin(theta));

		const Eigen::Vector2d v_xy = xy(t);
		const double x = v_xy[0];
		const double y = v_xy[1];

		const double d1 = (Eigen::Vector2d(-a, 0.) - r).norm();
		const double d2 = (v_xy - r).norm();

		return k * R * ((1 - b / d1) * a * sin(theta) - (1 - b / d2) * (x * sin(theta) - y * cos(theta)));
	};
}

DifferentialEquation<2> make_de(const double a, const double b, const double R, const double k,
        const double I, const double gamma,
        const std::function<Eigen::Vector2d(double)>& xy)
{
	const auto F = make_F(a, b, R, k, xy);

	return [I, gamma, F] (const double t, const Eigen::Vector2d& x)
	{
		return Eigen::Vector2d(x[1],
		                       1 / I * (-gamma * x[1] + F(t, x[0])));
	};
}
