#!/usr/bin/env python3

import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

data = np.loadtxt(sys.argv[1], skiprows=1)

x = data[:,0]
θ = data[:,1:]

x, θ = np.broadcast_arrays(x, θ.T)

fig, ax = plt.subplots(1, 1)

ax.plot(x.flatten(), θ.flatten(), ',')

ax.grid()
ax.set_xlabel(r'$\theta$')
ax.set_ylabel(r'$\omega$')

fig.savefig('{}.pdf'.format(sys.argv[1].rsplit('.', 1)[0]))
