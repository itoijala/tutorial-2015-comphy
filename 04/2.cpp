#include <cmath>
#include <cstdio>
#include <functional>
#include <string>
#include <vector>

#include <Eigen/Core>

#include "rungeKutta.hpp"
#include "utils.hpp"
#include "zeeman.hpp"

using std::function;
using std::string;

using Eigen::Vector2d;

function<Vector2d(double)> make_xy(const double x, const double A, const double Omega)
{
	return [x, A, Omega] (const double t)
	{
		return Vector2d(x,
		                A * sin(Omega * t));
	};
}

void work_a(const string& name, const double a, const double b, const double R, const double k,
        const double I, const double gamma,
        const function<Vector2d(double)>& xy)
{
	const double h = 1e-3;
	const double T = 1000.;

	const auto de = make_de(a, b, R, k, I, gamma, xy);

	auto file = fopen(name.c_str(), "w");
	fprintf(file, "# t θ ω\n");

	solve_write(file, de, h, 0., T, Vector2d(0., 0.));

	fclose(file);
}

void work_b(const double a, const double b, const double R, const double k,
        const double I, const double gamma,
        const double A, const double Omega)
{
	const double h = 1e-3;
	const double T = 2. * M_PI / Omega;
	const size_t points = 1e4;

	const auto de = make_de(a, b, R, k, I, gamma, make_xy(22., A, Omega));

	auto file = fopen("2b.dat", "w");
	fprintf(file, "# t θ ω\n");

	double t = 0.;
	Vector2d to(0., 0.);
	fprintf(file, "%.20e", t);
	print(file, to);
	fprintf(file, "\n");

	for (size_t i = 1; i <= points; ++i)
	{
		for (; t <= double(i) * T; t += h)
		{
			to = rungeKutta4(de, to, t, h);
		}

		fprintf(file, "%.20e", t);
		print(file, to);
		fprintf(file, "\n");

		fprintf(stderr, "%zu / %zu\n", i, points);
	}

	fclose(file);
}

void work_c(const double a, const double b, const double R, const double k,
        const double I, const double gamma,
        const double A, const double Omega)
{
	const double h = 1e-3;
	const double T = 2. * M_PI / Omega;
	const size_t theta_points = 1e2;

	auto file = fopen("2c.dat", "w");
	fprintf(file, "# x θ...\n");

	for (double x = 21.8; x <= 22.25; x+= 1e-3)
	{
		fprintf(file, "%.20e", x);

		const auto de = make_de(a, b, R, k, I, gamma, make_xy(x, A, Omega));

		double t = 0.;
		Vector2d to(0., 0.);

		for (size_t i = 1; i <= theta_points; ++i)
		{
			for (; t <= double(i) * T; t += h)
			{
				to = rungeKutta4(de, to, t, h);
			}

			if (i > 10)
			{
				fprintf(file, " %.20e", to[0]);
			}
		}

		fprintf(file, "\n");
		fprintf(stderr, "%.5f\n", x);
	}

	fclose(file);
}

int main(int argc, char** argv)
{
	const double a = 20.;
	const double b = 10.;
	const double R = 5.;
	const double k = 250.;
	const double I = 6250.;
	const double gamma = 2000.;

	const double A = 2.5;
	const double Omega = 2. * M_PI / 10.;

	if (argc == 2)
	{
		if (string(argv[1]) == "b")
		{
			work_b(a, b, R, k, I, gamma, A, Omega);
		}
		else if (string(argv[1]) == "c")
		{
			work_c(a, b, R, k, I, gamma, A, Omega);
		}
		else
		{
			const double x = string_to_double(argv[1]);

			char name[256];
			snprintf(name, 256, "2a_%.3f.dat", x);

			work_a(name, a, b, R, k, I, gamma, make_xy(x, A, Omega));
		}
	}
	else
	{
		fprintf(stderr, "Usage: %s <x>|b|c\n", argv[0]);
		return 1;
	}
}
