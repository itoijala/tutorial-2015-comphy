#include <algorithm>
#include <cctype>
#include <cstdio>
#include <stdexcept>
#include <string>

#include <Eigen/Core>

#include "rungeKutta.hpp"

#ifndef UTILS_H
#define UTILS_H

// Whitespace at front and back is ok.
double string_to_double(const std::string& s)
{
	size_t i = 0;
	double r = std::stod(s, &i);
	std::string rest = s.substr(i, std::string::npos);
	if (i != s.length() && !std::all_of(rest.begin(), rest.end(), isspace))
	{
		throw std::invalid_argument(__FUNCTION__ + (": " + s));
	}
	return r;
}

template<int D>
void print(FILE* file, const Vector<D>& v)
{
	for (int i = 0; i < D; ++i)
	{
		fprintf(file, " %.20e", v[i]);
	}
}

template<int D>
Vector<D> solve_write(FILE* file, const DifferentialEquation<D>& de, const double h, const double t0, const double T, const Vector<D>& x0)
{
	double t = t0;
	Vector<D> x = x0;
	fprintf(file, "%.20e", t);
	print(file, x);
	fprintf(file, "\n");

	for (; t <= T; t += h)
	{
		x = rungeKutta4(de, x, t, h);

		fprintf(file, "%.20e", t);
		print(file, x);
		fprintf(file, "\n");
	}

	return x;
}

#endif // UTILS_H
