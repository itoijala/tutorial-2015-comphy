#include <functional>

#include <Eigen/Core>

#ifndef RUNGEKUTTA_H
#define RUNGEKUTTA_H

template<int D>
using Vector = Eigen::Matrix<double, D, 1>;

template<int D>
using DifferentialEquation = std::function<Vector<D>(double t, const Vector<D>& x)>;

template<int D>
using Solver = std::function<Vector<D>(
        const DifferentialEquation<D>& f,
        const Vector<D>& x0, double t0, double h)>;

template<int D, int O>
Vector<D> rungeKutta(const DifferentialEquation<D>& f,
        const Vector<D>& x0, const double t0, const double h,
        const Eigen::Matrix<double, O, O>& a, const Vector<O>& b, const Vector<O>& c)
{
	Eigen::Matrix<double, D, O> k;

	for(int j = 0; j < O; ++j)
	{
		k.col(j) = h * f(t0 + h * c[j], x0 + (k * a.row(j).asDiagonal()).rowwise().sum());
	}

	return x0 + (k * b.asDiagonal()).rowwise().sum();
}

template<int D>
Vector<D> rungeKutta4(const DifferentialEquation<D>& f,
        const Vector<D>& x0, const double t0, const double h)
{
	Eigen::Matrix4d a;
	a << 0.   , 0.   , 0., 0.,
	     1./2., 0.   , 0., 0.,
	     0.   , 1./2., 0., 0.,
	     0.   , 0.   , 1., 0.;

	Eigen::Vector4d b;
	b << 1./6.,
	     1./3.,
	     1./3.,
	     1./6.;

	Eigen::Vector4d c;
	c << 0.,
	     1./2.,
	     1./2.,
	     1.;

	return rungeKutta(f, x0, t0, h, a, b, c);
}

#endif //RUNGEKUTTA_H
