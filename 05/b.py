#!/usr/bin/env python3

import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

t, v_cm, E_kin, E_pot, E_tot, T = np.loadtxt(sys.argv[1], unpack=True)

fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, 1)
fig.set_figheight(4 * fig.get_figheight())

ax1.plot(t, v_cm)

ax1.set_xlabel(r'$t$')
ax1.set_ylabel(r'$v_\text{cm}$')

ax2.plot(t, E_tot - np.mean(E_tot))

ax2.set_xlabel(r'$t$')
ax2.set_ylabel(r'$E_\text{tot} - \langle E_\text{tot} \rangle$')

ax3.plot(t, E_kin, label=r'$E_\text{kin}$')
ax3.plot(t, E_pot, label=r'$E_\text{pot}$')
ax3.plot(t, E_tot, label=r'$E_\text{tot}$')

ax3.set_xlabel(r'$t$')
ax3.legend(loc='best')

ax4.plot(t, T)

ax4.set_xlabel(r'$t$')
ax4.set_ylabel(r'$T$')

fig.savefig('{}.pdf'.format(sys.argv[1].rsplit('.', 1)[0]))
