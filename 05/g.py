#!/usr/bin/env python3

import sys

import matplotlib as mpl
import matplotlib.pylab as plt
import numpy as np

many = len(sys.argv) > 2

fig, ax = plt.subplots(1, 1)

for data in sys.argv[1:]:
    r, g = np.loadtxt(data, unpack=True)

    ax.plot(r, g, alpha=(0.5 if many else 1), label=r'$T = {}$'.format(data.rsplit('.', 1)[0].split('g_', 1)[1]))

if many:
    ax.legend(loc='best')

ax.set_xlabel(r'$r$')
ax.set_ylabel(r'$g(r)$')

fig.savefig('{}.pdf'.format('g-all' if many else sys.argv[1].rsplit('.', 1)[0]))
