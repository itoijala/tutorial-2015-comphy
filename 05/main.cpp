#include <cctype>
#include <cmath>
#include <cstdio>
#include <functional>
#include <random>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

#include <Eigen/Core>

using std::string;
using std::tuple;
using std::vector;

using Eigen::Vector2d;
using Eigen::VectorXd;

template<int D>
using Vector = Eigen::Matrix<double, D, 1>;

template<int D>
using Force = std::function<Vector<D>(const Vector<D>&)>;

using Potential = std::function<double(double)>;

template<int D>
Vector<D> difference(const double L, const Vector<D>& r1, const Vector<D>& r2)
{
	return (r2 - r1).unaryExpr([L] (const double x)
	{
		return x - L * floor(x / L + 0.5);
	});
}

template<int D>
vector<Vector<D>> forces(const Force<D>& force, double L, double C, const vector<Vector<D>>& r)
{
	const size_t N = r.size();
	vector<Vector<D>> F(N, Vector<D>::Zero());

	for (size_t i = 0; i < N; ++i)
	{
		for (size_t j = 0; j < i; ++j)
		{
			Vector<D> diff = difference(L, r[i], r[j]);
			if (diff.norm() > C)
			{
				continue;
			}
			Vector<D> f = force(diff);
			F[i] -= f;
			F[j] += f;
		}
	}

	return F;
}

template<int D>
tuple<vector<Vector<D>>, vector<Vector<D>>, vector<Vector<D>>> MDsimulation(
        const Force<D>& force, const double h, const double L, const double C,
        const vector<Vector<D>>& r0, const vector<Vector<D>>& v0, const vector<Vector<D>>& F0)
{
	const size_t N = r0.size();
	vector<Vector<D>> r(N);

	for (size_t i = 0; i < N; ++i)
	{
		r[i] = (r0[i] + h * v0[i] + 0.5 * h * h * F0[i]).unaryExpr([L] (const double x)
		{
			return x - L * floor(x / L);
		});
	}

	vector<Vector<D>> F = forces(force, L, C, r);
	vector<Vector<D>> v(N);

	for (size_t i = 0; i < N; ++i)
	{
		v[i] = v0[i] + 0.5 * h * (F[i] + F0[i]);
	}

	return make_tuple(r, v, F);
}

template<int D>
Vector<D> calc_v_cm(const vector<Vector<D>>& v)
{
	const size_t N = v.size();
	Vector<D> sum = Vector<D>::Zero();

	for (size_t i = 0; i < N; ++i)
	{
		sum += v[i] / double(N);
	}

	return sum;
}

template<int D>
double calc_E_pot(const Potential& potential, const double L, const double C, const vector<Vector<D>>& r)
{
	const size_t N = r.size();
	double sum = 0;

	for (size_t i = 0; i < N; ++i)
	{
		for (size_t j = 0; j < i; ++j)
		{
			Vector<D> diff = difference(L, r[i], r[j]);
			double d = diff.norm();
			if (d > C)
			{
				continue;
			}
			sum += potential(d);
		}
	}

	return sum;
}

template<int D>
double calc_E_kin(const vector<Vector<D>>& v)
{
	const size_t N = v.size();
	double sum = 0;

	for (size_t i = 0; i < N; ++i)
	{
		sum += v[i].squaredNorm();
	}

	return 0.5 * sum;
}

template<int D>
VectorXd calc_g(const double L, const int bins, const vector<Vector<D>>& r)
{
	const size_t N = r.size();
	VectorXd g = VectorXd::Zero(bins);

	for (size_t i = 0; i < N; ++i)
	{
		for (size_t j = 0; j < i; ++j)
		{
			Vector<D> diff = difference(L, r[i], r[j]);
			double d = diff.norm();
			if (d < 0.5 * L)
			{
				g[int(floor(bins * d / (0.5 * L)))]++;
			}
		}
	}

	return g;
}

template<int D>
Vector<D> Lennard_Jones_force(const Vector<D>& diff)
{
	double r = diff.norm();
	return 24. * (2. * pow(r, -14) - pow(r, -8)) * diff;
}

double Lennard_Jones_potential(const double r)
{
	return 4. * (pow(r, -12) - pow(r, -6));
}

tuple<vector<Vector2d>, vector<Vector2d>, vector<Vector2d>, double, double, double, double, double>
initialise(const double L, const size_t sqrtN, const double C, const double T0)
{
	const size_t D = 2;
	const size_t N = sqrtN * sqrtN;

	vector<Vector2d> r0(N);
	for (size_t m = 0; m < sqrtN; ++m)
	{
		for (size_t n = 0; n < sqrtN; ++n)
		{
			r0[sqrtN * m + n] = Vector2d(L / (2. * double(sqrtN)) * (1. + 2. * double(m)),
			                             L / (2. * double(sqrtN)) * (1. + 2. * double(n)));
		}
	}

	std::mt19937 generator;
	std::normal_distribution<double> distribution(-1., 1.);

	vector<Vector2d> v0(N);
	for (size_t i = 0; i < N; ++i)
	{
		v0[i] = Vector2d(distribution(generator),
		                 distribution(generator));
	}

	const Vector2d v_cm0 = calc_v_cm(v0);
	for (size_t i = 0; i < N; ++i)
	{
		v0[i] -= v_cm0;
	}

	const double E_kin0 = calc_E_kin(v0);
	for (size_t i = 0; i < N; ++i)
	{
		v0[i] *= sqrt(0.5 * T0 / E_kin0 * D * double(N - 1));
	}

	const double v_cm = calc_v_cm(v0).norm();

	const double E_pot = calc_E_pot(Lennard_Jones_potential, L, C, r0);

	const double E_kin = calc_E_kin(v0);

	const vector<Vector2d> F0 = forces<2>(Lennard_Jones_force<2>, L, C, r0);

	return make_tuple(r0, v0, F0, v_cm, E_kin, E_pot, E_kin + E_pot, 2. / double(D * (N - 1)) * E_kin);
}

void work_b(const double T0, const size_t time)
{
	const size_t D = 2;
	const size_t sqrtN = 6;
	const size_t N = sqrtN + sqrtN;
	const double L = 8;
	const double C = 0.5 * L;
	const double h = 0.001;

	vector<Vector2d> r0, v0, F0;
	double v_cm, E_kin, E_pot, E_tot, T;
	tie(r0, v0, F0, v_cm, E_kin, E_pot, E_tot, T) = initialise(L, sqrtN, C, T0);

	size_t t = 0;

	char name[256];
	snprintf(name, 256, "b_%.3f_%zu.dat", T0, time);
	auto file = fopen(name, "w");
	fprintf(file, "# t v_cm E_kin E_pot E_tot T\n");

	fprintf(file, "%zu %.20e %.20e %.20e %.20e %.20e\n", t, v_cm, E_kin, E_pot, E_tot, T);

	for (t = 1; t < time; ++t)
	{
		vector<Vector2d> r, v, F;
		tie(r, v, F) = MDsimulation<2>(Lennard_Jones_force<2>, h, L, C, r0, v0, F0);
		r0 = r;
		v0 = v;
		F0 = F;

		v_cm = calc_v_cm(v).norm();
		E_kin = calc_E_kin(v);
		E_pot = calc_E_pot(Lennard_Jones_potential, L, C, r);
		E_tot = E_kin + E_pot;
		T = 2. / double(D * (N - 1)) * E_kin;

		if (t % (time / 2000) == 0)
		{
			fprintf(file, "%zu %.20e %.20e %.20e %.20e %.20e\n", t, v_cm, E_kin, E_pot, E_tot, T);
		}

		if (t % (time / 100) == 0)
		{
			fprintf(stderr, "%zu %%\n", t * 100 / time);
		}
	}

	fclose(file);
}

void work_g(const double T0)
{
	const size_t sqrtN = 6;
	const double L = 8;
	const size_t D = 2;
	const size_t N = sqrtN * sqrtN;
	const double C = 0.5 * L;
	const size_t time = 1e6;
	const double h = 0.001;

	const size_t time_equi = 1e4;
	const int bins = 1000;

	vector<Vector2d> r0, v0, F0;
	double v_cm, E_kin, E_pot, E_tot, T;
	tie(r0, v0, F0, v_cm, E_kin, E_pot, E_tot, T) = initialise(L, sqrtN, C, T0);

	VectorXd g = VectorXd::Zero(bins);

	for (size_t t = 1; t < time; ++t)
	{
		vector<Vector2d> r, v, F;
		tie(r, v, F) = MDsimulation<2>(Lennard_Jones_force<2>, h, L, C, r0, v0, F0);
		r0 = r;
		v0 = v;
		F0 = F;

		if (t >= time_equi)
		{
			g += (calc_g(L, bins, r) - g) / double(t - time_equi + 1);
		}

		if (t % (time / 100) == 0)
		{
			fprintf(stderr, "%zu %%\n", t * 100 / time);
		}
	}

	char name[256];
	snprintf(name, 256, "g_%.5f.dat", T0);
	auto file = fopen(name, "w");
	fprintf(file, "# b g\n");

	const double rho = N / pow(L, D);
	const double Delta_V = pow(M_PI, D / 2.) / tgamma(D / 2. + 1.) * pow(0.5 * L / bins, D);

	for (int b = 0; b < bins; ++b)
	{
		const double r = double(b) / bins * 0.5 * L;
		// 0.5: double count pairs
		g[b] /= 0.5 * rho * N * Delta_V * (pow(double(b + 1), D) - pow(double(b), D));
		fprintf(file, "%.20e %.20e\n", r, g[b]);
	}

	fclose(file);
}

double string_to_double(const string& s)
{
	size_t i = 0;
	double r = std::stod(s, &i);
	if (i != s.length() || isspace(s[0]))
	{
		throw std::invalid_argument(__FUNCTION__ + (": '" + s + "'"));
	}
	return r;
}

size_t string_to_size_t(const string& s)
{
	size_t i = 0;
	size_t r = std::stoull(s, &i);
	if (i != s.length() || isspace(s[0]))
	{
		throw std::invalid_argument(__FUNCTION__ + (": '" + s + "'"));
	}
	return r;
}

int main(int argc, char**argv)
{
	if (argc == 2)
	{
		const double T = string_to_double(argv[1]);

		work_g(T);
	}
	else if (argc == 3)
	{
		const double T = string_to_double(argv[1]);
		const size_t time = string_to_size_t(argv[2]);

		work_b(T, time);
	}
	else
	{
		fprintf(stderr, "Usage: %s <T> [<time>]\n", argv[0]);
		return 1;
	}
}
