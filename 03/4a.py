#!/usr/bin/env python3

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

def looping(b, u):
    c = (u**2 - 3) / (u**2 + 1)
    return ( b * u * c,
            -b *     c)

fig, ax = plt.subplots(1, 1)

u = np.linspace(-5, 5, 1000)

for b in reversed([1, 2, 3]):
    x, y = looping(b, u)
    ax.plot(x, y, '-', label=r'$b = {}$'.format(b))

ax.set_aspect('equal')
ax.legend(loc='best')
ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$y$')

fig.savefig('4a.pdf')
