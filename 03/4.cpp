#include <cstdio>
#include <functional>
#include <string>
#include <tuple>
#include <vector>

#include <Eigen/Core>

#include "AutoDiffScalar.h"

#include "rungeKutta.hpp"
#include "utils.hpp"

using std::function;
using std::string;
using std::tuple;
using std::vector;

using Eigen::Matrix;
using Eigen::Vector2d;

using Eigen::AutoDiffScalar;

template <template <class> class F, typename... Args>
function<tuple<Vector2d, Vector2d, Vector2d>(double)> calculatePoint(Args... args)
{
	typedef double Type0;
	typedef Type0 Derivative0;
	typedef Matrix<Derivative0, 1, 1> Type1;
	typedef AutoDiffScalar<Type1> Derivative1;
	typedef Matrix<Derivative1, 1, 1> Type2;
	typedef AutoDiffScalar<Type2> Derivative2;

	const auto fun = F<Derivative2>()(args...);

	return [fun] (const double u)
	{
		Derivative2 z;
		z.value() = u;
		z.derivatives()(0) = 1.;
		z.value().derivatives()(0) = 1.;
		z.derivatives()(0).derivatives()(0) = 0.;

		const auto point = fun(z);

		const Vector2d x(point(0).value().value(),
		                 point(1).value().value());
		const Vector2d dx(point(0).derivatives()(0).value(),
		                  point(1).derivatives()(0).value());
		const Vector2d ddx(point(0).derivatives()(0).derivatives()(0),
		                   point(1).derivatives()(0).derivatives()(0));

		return tuple<Vector2d, Vector2d, Vector2d>(x, dx, ddx);
	};
}

double calc_ddu(const double v, const Vector2d& x, const Vector2d& dx, const Vector2d& ddx)
{
	return - ((dx[0] * ddx[0] + dx[1] * ddx[1]) * v * v + dx[1]) / (dx[0] * dx[0] + dx[1] * dx[1]);
}

template <template <class> class F, typename... Args>
DifferentialEquation<2> fromParameterisation(Args... args)
{
	const auto calc = calculatePoint<F>(args...);

	return [calc] (const double t, const Vector2d& uv)
	{
		const double u = uv(0);
		const double v = uv(1);

		Vector2d x, dx, ddx;
		std::tie(x, dx, ddx) = calc(u);

		return Vector2d(v, calc_ddu(v, x, dx, ddx));
	};
}

template <template <class> class F, typename... Args>
void cef(const string& name, const size_t n, const double u0, const double v0, const double T, Args... args)
{
	vector<double> t;
	vector<Vector2d> uv;
	std::tie(t, uv) = rungeKutta4<2>(fromParameterisation<F>(args...), Vector2d(u0, v0), n, 0., T);

	const auto file = fopen(name.c_str(), "w");
	fprintf(file, "# t u v x y dx dy ddx ddy Nx Ny N θ\n");

	const auto calc = calculatePoint<F>(args...);

	for (size_t i = 0; i <= n; ++i)
	{
		const double u = uv[i][0];
		const double v = uv[i][1];

		fprintf(file, "%.20e", t[i]);
		print(file, uv[i]);

		Vector2d x, dx, ddx;
		std::tie(x, dx, ddx) = calc(u);

		print(file, x);
		print(file, dx);
		print(file, ddx);

		const double ddu = calc_ddu(v, x, dx, ddx);
		const Vector2d a = ddx * v * v + dx * ddu;
		const Vector2d N = a + Vector2d(0., 1.);
		const double theta = acos(N.dot(dx) / N.norm() / dx.norm());

		print(file, N);
		fprintf(file, " %.20e %.20e", N.norm(), theta);

		fprintf(file, "\n");
	}

	fclose(file);
}

double d_work(const DifferentialEquation<2>& force, const double v)
{
	const Vector2d uv0(-5., v);

	size_t n = 5000;
	double T = 0.75;

	double u = 0.;

	while (fabs(u) < 1.)
	{
		n *= 2;
		T *= 2.;

		vector<double> t;
		vector<Vector2d> uv;
		std::tie(t, uv) = rungeKutta4(force, uv0, n, 0., T);

		u = uv[n][0];

		if (u < 0)
		{
			bool ok = false;
			for (size_t i = 0; i < n; ++i)
			{
				if (uv[i][0] > u)
				{
					ok = true;
					break;
				}
			}
			if (!ok)
			{
				u = 0;
			}
		}
	}

	printf("n = %zu\n", n);
	printf("T = %.5f\n", T);
	printf("u = %.5f\n", u);

	return u;
}

template <template <class> class F, typename... Args>
void d(double v_min, double v_max, Args... args)
{
	const double epsilon = 1e-5;

	const auto force = fromParameterisation<F>(args...);

	while ((v_max - v_min) / v_max >= epsilon)
	{
		const double v = 0.5 * (v_max + v_min);

		const double u = d_work(force, v);
		if (u < 0)
		{
			v_min = v;
		}
		else if (u > 0)
		{
			v_max = v;
		}
		else
		{
			fprintf(stderr, "u = 0!\n");
			throw u;
		}

		printf("v = %.5f\n", v);
		printf("\n");
	}
}

template <typename T>
class Looping
{
public:
	function<Matrix<T, 2, 1>(T const &)> operator()(const double b)
	{
		return [b] (const T& u)
		{
			Matrix<T, 2, 1> y;
			y << u * (u * u - 3) * b / (u * u + 1),
			     - (u * u - 3) * b / (u * u + 1);
			return y;
		};
	}
};

template <typename T>
class Circle
{
public:
	function<Matrix<T, 2, 1>(T const &)> operator()(const double R)
	{
		return [R] (const T& u)
		{
			Matrix<T, 2, 1> y;
			y << sin(u) * R,
			     cos(u) * R;
			return y;
		};
	}
};

int main(int argc, char** argv)
{
	if (argc == 4)
	{
		const size_t n = std::stoul(argv[1]);
		const double v0 = std::stod(argv[2]);
		const double T = std::stod(argv[3]);

		char name[256];
		snprintf(name, 256, "4-%zu-%.5f-%.1f.dat", n, v0, T);

		cef<Looping>(name, n, -5., v0, T, 1.);
	}
	else if (argc == 1)
	{
		d<Looping>(0.1, 10., 1.);
	}
	else if (argc == 5 && string(argv[1]) == "circle")
	{
		const size_t n = std::stoul(argv[2]);
		const double v0 = std::stod(argv[3]);
		const double T = std::stod(argv[4]);

		char name[256];
		snprintf(name, 256, "4-circle-%zu-%.5f-%.1f.dat", n, v0, T);

		cef<Circle>(name, n, -M_PI, v0, T, 10.);
	}
	else
	{
		fprintf(stderr, "Usage: %s [[circle] <n> <v0> <T>]\n", argv[0]);
	}
}
