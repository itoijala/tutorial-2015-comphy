#include <cstdio>
#include <functional>
#include <vector>
#include <tuple>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include "rungeKutta.hpp"
#include "utils.hpp"

using std::function;
using std::string;
using std::vector;

using Eigen::Vector3d;

template<int D>
function<Vector<D>(double, const Vector<D>&)> gravity(const double alpha)
{
	return [alpha] (const double t, const Vector<D>& r)
	{
		return - alpha * r / pow(r.norm(), alpha + 2);
	};
}

void abce(const string& name, const double alpha, const Vector3d& r0, const Vector3d& v0, size_t N, double T)
{
	vector<double> t;
	vector<Vector3d> r;
	vector<Vector3d> v;
	std::tie(t, r, v) = newton<3>(rungeKutta4<6>, gravity<3>(alpha), r0, v0, N, 0., T);

	auto file = fopen(name.c_str(), "w");
	fprintf(file, "# t r1 r2 r3 v1 v2 v3 E_kin E_pot E_tot L1 L2 L3 Λ1 Λ2 Λ3\n");

	for (size_t i = 0; i <= N; ++i)
	{
		fprintf(file, "%.20e", t[i]);
		print(file, r[i]);
		print(file, v[i]);

		const double E_kin = 0.5 * v[i].squaredNorm();
		const double E_pot = -1. / r[i].norm();
		const double E_tot = E_kin + E_pot;
		fprintf(file, " %.20e %.20e %.20e", E_kin, E_pot, E_tot);

		const Vector3d L = r[i].cross(v[i]);
		print(file, L);

		const Vector3d Lambda = v[i].cross(L) - r[i].normalized();
		print(file, Lambda);

		fprintf(file, "\n");
	}

	fclose(file);
}

void d()
{
	const double T_min = 10.;
	const double T_max = 10000.;
	const size_t P = 20;
	const size_t N = 100;

	const Vector3d r0(1., 0., 0.);
	const Vector3d v0(0., 1.3, 0.);

	auto file = fopen("3d.dat", "w");
	fprintf(file, "# T dx dy dz dvx dvy dvz\n");

	for (size_t i = 0; i <= P; ++i)
	{
		const double T = T_min + (T_max - T_min) / double(P) * double(i);
		const size_t n = N * size_t(T);

		vector<double> t;
		vector<Vector3d> r;
		vector<Vector3d> v;
		std::tie(t, r, v) = newton<3>(rungeKutta4<6>, gravity<3>(1.), r0, v0, n, 0., T);

		std::tie(t, r, v) = newton<3>(rungeKutta4<6>, gravity<3>(1.), r[n], -v[n], n, 0., T);

		const Vector3d dr = (r0 - r[n]).cwiseAbs();
		const Vector3d dv = (v0 + v[n]).cwiseAbs();

		fprintf(file, "%.20e", T);
		print(file, dr);
		print(file, dv);
		fprintf(file, "\n");
	}

	fclose(file);
}

int main(int argc, char** argv)
{
	if (argc == 5)
	{
		const double alpha = std::stod(argv[1]);
		const Vector3d v0(0., std::stod(argv[2]), 0.);
		const double T = std::stod(argv[3]);
		const size_t N = std::stoul(argv[4]);

		char name[256];
		snprintf(name, 256, "3-%.1f-%.2f-%.0f-%zu.dat", alpha, v0[1], T, N);

		abce(name, alpha, Vector3d(1., 0., 0.), v0, N, T);
	}
	else if (argc == 1)
	{
		d();
	}
	else
	{
		fprintf(stderr, "Usage: %s [<α> <v20> <T> <N>]", argv[0]);
		return 1;
	}
}
