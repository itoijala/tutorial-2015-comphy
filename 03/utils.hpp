#include <cstdio>

#include "rungeKutta.hpp"

#ifndef UTILS_H
#define UTILS_H

template<int D>
void print(FILE* file, const Vector<D>& v)
{
	for (int i = 0; i < D; ++i)
	{
		fprintf(file, " %.20e", v[i]);
	}
}

Vector<1> vec(const double x)
{
	Vector<1> v;
	v << x;
	return v;
}

#endif // UTILS_H
