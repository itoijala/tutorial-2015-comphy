#!/usr/bin/env python3

import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

t, *rv, E_kin, E_pot, E_tot = np.loadtxt(sys.argv[1], unpack=True)

fig, (*ax, axE) = plt.subplots(len(rv) // 2 + 1, 1)
fig.set_figheight((len(rv) // 2 + 1) * fig.get_figheight())

for i in range(len(rv) // 2):
    r, v = rv[i], rv[i + len(rv) // 2]
    ax[i].plot(t, r, label=r'$r_{}$'.format(i + 1))
    ax[i].plot(t, v, label=r'$v_{}$'.format(i + 1))
    ax[i].legend(loc='best')

axE.plot(t, E_kin, label=r'$E_\text{kin}$')
axE.plot(t, E_pot, label=r'$E_\text{pot}$')
axE.plot(t, E_tot, label=r'$E_\text{tot}$')
axE.legend(loc='best')

fig.savefig('2-{}.pdf'.format(sys.argv[1].split('-', 1)[1].rsplit('.', 1)[0]))
