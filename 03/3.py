#!/usr/bin/env python3

import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

data = np.loadtxt(sys.argv[1], skiprows=1)
data = data.T

t = data[0]
r = data[1:4]
v = data[4:7]
E_kin = data[7]
E_pot = data[8]
E_tot = data[9]
L = data[10:13]
Λ = data[13:16]

fig, (ax_r, ax_v, ax_E, ax_L, ax_Λ) = plt.subplots(5, 1)
fig.set_figheight(5 * fig.get_figheight())

ax_r.plot(r[0], r[1])
ax_r.set_aspect('equal')
ax_r.grid()
ax_r.set_xlabel(r'$x$')
ax_r.set_ylabel(r'$y$')

ax_v.plot(v[0], v[1])
ax_v.set_aspect('equal')
ax_v.grid()
ax_v.set_xlabel(r'$v_x$')
ax_v.set_ylabel(r'$v_y$')

ax_E.plot(t, E_kin, label=r'$E_\text{kin}$')
ax_E.plot(t, E_pot, label=r'$E_\text{pot}$')
ax_E.plot(t, E_tot, label=r'$E_\text{tot}$')
ax_E.legend(loc='best')
ax_E.set_xlabel(r'$t$')

ax_L.plot(t, L[0], label=r'$L_1$')
ax_L.plot(t, L[1], label=r'$L_2$')
ax_L.plot(t, L[2], label=r'$L_3$')
ax_L.legend(loc='best')
ax_L.set_xlabel(r'$t$')

ax_Λ.plot(t, Λ[0], label=r'$\Lambda_1$')
ax_Λ.plot(t, Λ[1], label=r'$\Lambda_2$')
ax_Λ.plot(t, Λ[2], label=r'$\Lambda_3$')
ax_Λ.legend(loc='best')
ax_Λ.set_xlabel(r'$t$')

fig.savefig('{}.pdf'.format(sys.argv[1].rsplit('.', 1)[0]))
