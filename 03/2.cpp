#include <cstdio>
#include <string>
#include <vector>
#include <tuple>

#include <Eigen/Core>

#include "rungeKutta.hpp"
#include "utils.hpp"

using std::string;
using std::vector;

template<int D>
Vector<D> harmonic(const double t, const Vector<D>& r)
{
	return -r;
}

template<int D>
void work(const string& name, const Vector<D>& r0, const Vector<D>& v0, const size_t N, const double T)
{
	vector<double> t;
	vector<Vector<D>> r;
	vector<Vector<D>> v;
	std::tie(t, r, v) = newton<D>(rungeKutta4<2 * D>, harmonic<D>, r0, v0, N, 0., T);

	auto file = fopen(name.c_str(), "w");
	fprintf(file, "# t r... v... E_kin E_pot E_tot\n");

	for (size_t i = 0; i <= N; ++i)
	{
		fprintf(file, "%.20e", t[i]);
		print(file, r[i]);
		print(file, v[i]);

		const double E_kin = 0.5 * v[i].squaredNorm();
		const double E_pot = 0.5 * r[i].squaredNorm();
		const double E_tot = E_kin + E_pot;
		fprintf(file, " %.20e %.20e %.20e\n", E_kin, E_pot, E_tot);
	}

	fclose(file);
}

int main(int argc, char** argv)
{
	if (argc == 3)
	{
		const double T = std::stod(argv[1]);
		const size_t N = std::stoul(argv[2]);

		char name[256];
		snprintf(name, 256, "2-%.0f-%zu.dat", T, N);

		work(name, vec(1.), vec(0.), N, T);
	}
	else if (argc == 1)
	{
		const size_t T = 10;
		const size_t N = 100;
		work("2-2d.dat", Eigen::Vector2d(1., 0.), Eigen::Vector2d(0.5, 0.5), N, T);
	}
	else
	{
		fprintf(stderr, "Usage: %s [<T> <N>]\n", argv[0]);
		return 1;
	}
}
