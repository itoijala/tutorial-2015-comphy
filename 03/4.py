#!/usr/bin/env python3

import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

t, u, v, x, y, dx, dy, ddx, ddy, Nx, Ny, N, θ = np.loadtxt(sys.argv[1], unpack=True, skiprows=1)

fig, ax = plt.subplots(7, 1)
fig.set_figheight(7 * fig.get_figheight())

s = len(u) // 101
ax[0].plot(x[::s], y[::s], 'b.')

ax[0].quiver(x[::s], y[::s], Nx[::s], Ny[::s], width=1, units='dots', color='k')#, angles='xy', scale_units='xy', scale=1)
ax[0].quiver(x[::s], y[::s], (dx * v)[::s], (dy * v)[::s], width=1, units='dots', color='r')#, angles='xy', scale_units='xy', scale=1)

ax[0].set_aspect('equal')
y_min = np.min(y)
y_max = np.max(y)
ax[0].set_ylim((1.1 if y_min < 0 else 0.9) * y_min, (0.9 if y_max < 0 else 1.1) * y_max)
ax[0].set_xlabel(r'$x$')
ax[0].set_ylabel(r'$y$')

ax[1].plot(t, u, label=r'$u$')
ax[1].plot(t, v, label=r'$v$')

ax[1].set_xlim(t[0], t[-1])
ax[1].legend(loc='best')
ax[1].set_xlabel(r'$t$')
ax[1].grid()

ax[2].plot(t, x, label=r'$x$')
ax[2].plot(t, y, label=r'$y$')

ax[2].set_xlim(t[0], t[-1])
ax[2].legend(loc='best')
ax[2].set_xlabel(r'$t$')
ax[2].grid()

ax[3].plot(t, dx, label=r"$x'$")
ax[3].plot(t, dy, label=r"$y'$")

ax[3].set_xlim(t[0], t[-1])
ax[3].legend(loc='best')
ax[3].set_xlabel(r'$t$')
ax[3].grid()

ax[4].plot(t, ddx, label=r"$x''$")
ax[4].plot(t, ddy, label=r"$y''$")

ax[4].set_xlim(t[0], t[-1])
ax[4].legend(loc='best')
ax[4].set_xlabel(r'$t$')
ax[4].grid()

ax[5].plot(t, Nx, label=r"$N_x$")
ax[5].plot(t, Ny, label=r"$N_y$")
ax[5].plot(t, N, label=r"$|N|$")

ax[5].set_xlim(t[0], t[-1])
ax[5].legend(loc='best')
ax[5].set_xlabel(r'$t$')
ax[5].grid()

ax[6].plot(t, θ - np.pi / 2)

ax[6].set_xlim(t[0], t[-1])
ax[6].set_xlabel(r'$t$')
ax[6].set_ylabel(r'$\displaystyle \theta - \frac{\pi}{2}$')

fig.savefig('{}.pdf'.format(sys.argv[1].rsplit('.', 1)[0]))
