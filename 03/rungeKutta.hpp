#include <tuple>
#include <vector>

#include <Eigen/Core>

#ifndef RUNGEKUTTA_H
#define RUNGEKUTTA_H

template<int D>
using Vector = Eigen::Matrix<double, D, 1>;

template<int D>
using DifferentialEquation = std::function<Vector<D>(double t, const Vector<D>& x)>;

template<int D>
using Solver = std::function<std::tuple<std::vector<double>, std::vector<Vector<D>>>(
       const DifferentialEquation<D>& f,
       const Vector<D>& x0, size_t N, double t0, double T)>;

template<int D, int O>
std::tuple<std::vector<double>, std::vector<Vector<D>>>
rungeKutta(const DifferentialEquation<D>& f,
           const Vector<D>& x0, const size_t N, const double t0, const double T,
           const Eigen::Matrix<double, O, O>& a, const Vector<O>& b, const Vector<O>& c)
{
	const double h = (T - t0) / double(N);

	std::vector<double> t(N + 1);
	std::vector<Vector<D>> x(N + 1, Vector<D>());

	t[0] = t0;
	x[0] = x0;

	for(size_t i = 1; i <= N; ++i)
	{
		Eigen::Matrix<double, D, O> k;

		for(int j = 0; j < O; ++j)
		{
			k.col(j) = h * f(t[i - 1] + h * c[j], x[i - 1] + (k * a.row(j).asDiagonal()).rowwise().sum());
		}
		x[i] = x[i - 1] + (k * b.asDiagonal()).rowwise().sum();
		t[i] = t[i - 1] + h;
	}

	return std::make_tuple(t, x);
}

template<int D>
std::tuple<std::vector<double>, std::vector<Vector<D>>>
rungeKutta4(const DifferentialEquation<D>& f,
            const Vector<D>& x0, const size_t N, const double t0, const double T)
{
	Eigen::Matrix4d a;
	a << 0.   , 0.   , 0., 0.,
	     1./2., 0.   , 0., 0.,
	     0.   , 1./2., 0., 0.,
	     0.   , 0.   , 1., 0.;

	Eigen::Vector4d b;
	b << 1./6.,
	     1./3.,
	     1./3.,
	     1./6.;

	Eigen::Vector4d c;
	c << 0.,
	     1./2.,
	     1./2.,
	     1.;

	return rungeKutta(f, x0, N, t0, T, a, b, c);
}

template<int D>
std::tuple<std::vector<double>, std::vector<Vector<D>>, std::vector<Vector<D>>>
newton(const Solver<2 * D>& solver, const DifferentialEquation<D>& F,
       const Vector<D>& r0, const Vector<D>& v0, const size_t N, const double t0, const double T)
{
	Vector<2 * D> x0;
	x0 << r0,
	      v0;

	std::vector<double> t;
	std::vector<Vector<2 * D>> x;
	std::tie(t, x) = solver([&F] (const double t, const Vector<2 * D>& x)
	{
		Vector<2 * D> y;
		y << x.template tail<D>(),
		     F(t, x.template head<D>());
		return y;
	}, x0, N, t0, T);

	std::vector<Vector<D>> r(N + 1, Vector<D>());
	std::vector<Vector<D>> v(N + 1, Vector<D>());
	for (size_t i = 0; i <= N; ++i)
	{
		r[i] = x[i].template head<D>();
		v[i] = x[i].template tail<D>();
	}

	return std::make_tuple(t, r, v);
}

#endif //RUNGEKUTTA_H
