#!/usr/bin/env python3

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

T, dx, dy, dz, dvx, dvy, dvz = np.loadtxt('3d.dat', unpack=True)

fig, ax = plt.subplots(1, 1)

ax.plot(T, dx, label=r'$\Delta x$')
ax.plot(T, dy, label=r'$\Delta y$')
ax.plot(T, dvx, label=r'$\Delta v_x$')
ax.plot(T, dvy, label=r'$\Delta v_y$')

ax.set_xscale('log')
ax.set_yscale('log')
ax.grid()
ax.legend(loc='best')
ax.set_xlabel(r'$T$')

fig.savefig('3d.pdf')
