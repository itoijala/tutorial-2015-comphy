#include <cmath>
#include <cstdio>
#include <functional>
#include <vector>

using std::function;
using std::vector;

// energy in units of μ_0/(4π) * a^(-3) * m^2
// a = lattice constant
// m = strength of one magnetic moment

double calc(const int N, const double theta_0, const function<double(int, int)>& calc_theta_m, const function<double(double, double, double, double)>& f)
{
	double sum = 0.;

	#pragma omp parallel for reduction(+:sum)
	for (int i = -N; i <= N; ++i)
	{
		for (int j = -N; j <= N; ++j)
		{
			if (i == 0 && j == 0)
			{
				continue;
			}

			double R = sqrt(i * i + j * j);
			double theta_R = atan2(i, j); // Definition of theta!
			double theta_m = calc_theta_m(i, j);

			sum += f(R, theta_R, theta_0, theta_m);
		}
	}

	return sum;
}

double calc_E(const int N, const double theta_0, const function<double(int, int)>& calc_theta_m)
{
	return calc(N, theta_0, calc_theta_m, [] (double R, double theta_R, double theta_0, double theta_m)
	{
		return 1. / (R * R * R) * (-3. * cos(theta_R - theta_0) * cos(theta_R - theta_m) + cos(theta_0 - theta_m));
	});
}

double calc_T_B(const int N, const double theta_0, const function<double(int, int)>& calc_theta_m)
{
	return calc(N, theta_0, calc_theta_m, [] (double R, double theta_R, double theta_0, double theta_m)
	{
		return -1. / (R * R * R) * (-3. * sin(theta_R - theta_0) * cos(theta_R - theta_m) - sin(theta_0 - theta_m));
	});
}

function<double(double)> derive(double h, const function<double(double)> f)
{
	return [f, h] (double x)
	{
		return (f(x + h) - f(x - h)) / (2. * h);
	};
}

double ferro(int i, int j)
{
	return 0.; // Definition of theta!
}

double antiferro(int i, int j)
{
	return ((i + j) % 2) == 0 ? 0. : M_PI; // Definition of theta!
}

void a()
{
	const size_t points = 100;
	const vector<int> N{2, 5, 10, 1000};

	auto file = fopen("1a.dat", "w");

	fprintf(file, "# N\n");
	for (auto n : N)
	{
		fprintf(file, " %d %d", n, -n);
	}
	fprintf(file, "\n");

	fprintf(file, "# theta_0 E(N)\n");

	for (size_t i = 0; i < points; ++i)
	{
		double theta_0 = 2. * M_PI / points * i;
		fprintf(file, "%.20e", theta_0);

		for (auto n : N)
		{
			fprintf(file, " %.20e %.20e", calc_E(n, theta_0, ferro), calc_E(n, theta_0, antiferro));
		}

		fprintf(file, "\n");
		printf("%zu\n", i);
	}

	fclose(file);
}

void b()
{
	const size_t points = 100;
	const double h = 0.001;
	const vector<int> N{2, 5, 10};

	auto file = fopen("1b.dat", "w");

	fprintf(file, "# N\n");
	for (auto n : N)
	{
		fprintf(file, " %d %d ", n, -n);
	}
	fprintf(file, "\n");

	fprintf(file, "# theta_0 T(N) T_B(N)\n");

	for (size_t i = 0; i < points; ++i)
	{
		double theta_0 = 2. * M_PI / points * i;
		fprintf(file, "%.20e", theta_0);

		for (auto n : N)
		{
			auto calc_T_ferro = derive(h, [n] (double theta_0) { return -calc_E(n, theta_0, ferro); });
			auto calc_T_antiferro = derive(h, [n] (double theta_0) { return -calc_E(n, theta_0, antiferro); });

			fprintf(file, " %.20e %.20e %.20e %.20e", calc_T_ferro(theta_0), calc_T_B(n, theta_0, ferro), calc_T_antiferro(theta_0), calc_T_B(n, theta_0, antiferro));
		}

		fprintf(file, "\n");
		printf("%zu\n", i);
	}

	fclose(file);
}

int main()
{
	a();
	b();
}
