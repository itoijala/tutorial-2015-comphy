#!/usr/bin/env python3

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

with open('1b.dat', 'rb') as f:
    N = np.genfromtxt(f.readlines()[:2], dtype=int)

theta, *T = np.loadtxt('1b.dat', skiprows=2, unpack=True)
T = np.array(T)

N_ferro = N[::2]
N_antiferro = N[1::2]
T_ferro = T[::4]
T_B_ferro = T[1::4]
T_antiferro = T[2::4]
T_B_antiferro = T[3::4]

fig, (ax1, ax2) = plt.subplots(2, 1)

for N, T, T_B in zip(N_ferro, T_ferro, T_B_ferro):
    ax1.plot(theta, T, label='$N = {:d}$, numeric'.format(abs(N)))
    ax1.plot(theta, T_B, ls='--', label='$N = {:d}$, analytic'.format(abs(N)))

for N, T, T_B in zip(N_antiferro, T_antiferro, T_B_antiferro):
    ax2.plot(theta, T, label='$N = {:d}$, numeric'.format(abs(N)))
    ax2.plot(theta, T_B, ls='--', label='$N = {:d}$, analytic'.format(abs(N)))

ax1.set_xlim(0, 2 * np.pi)
ax2.set_xlim(0, 2 * np.pi)

ax1.set_title('Ferromagnetic')
ax2.set_title('Antiferromagnetic')

ax1.set_xlabel(r'$\theta_0$')
ax1.set_ylabel(r'$T(N, \theta_0)$')
ax2.set_xlabel(r'$\theta_0$')
ax2.set_ylabel(r'$T(N, \theta_0)$')

ax1.legend(loc='upper left', bbox_to_anchor=(0.11, 1))
ax2.legend(loc='upper right', bbox_to_anchor=(0.89, 1))

fig.savefig('1b.pdf')
