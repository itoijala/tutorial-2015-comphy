#!/usr/bin/env python3

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

with open('1a.dat', 'rb') as f:
    N = np.genfromtxt(f.readlines()[:2], dtype=int)

theta, *E = np.loadtxt('1a.dat', skiprows=2, unpack=True)
E = np.array(E)

N_ferro = N[::2]
N_antiferro = N[1::2]
E_ferro = E[::2]
E_antiferro = E[1::2]

fig, (ax1, ax2) = plt.subplots(2, 1)

for N, E in zip(N_ferro, E_ferro):
    ax1.plot(theta, E, label='$N = {:d}$'.format(abs(N)))

for N, E in zip(N_antiferro, E_antiferro):
    ax2.plot(theta, E, label='$N = {:d}$'.format(abs(N)))

ax1.set_xlim(0, 2 * np.pi)
ax2.set_xlim(0, 2 * np.pi)

ax1.set_title('Ferromagnetic')
ax2.set_title('Antiferromagnetic')

ax1.set_xlabel(r'$\theta_0$')
ax1.set_ylabel(r'$E(N, \theta_0)$')
ax2.set_xlabel(r'$\theta_0$')
ax2.set_ylabel(r'$E(N, \theta_0)$')

ax1.legend(loc='best')
ax2.legend(loc='best')

fig.savefig('1a.pdf')
