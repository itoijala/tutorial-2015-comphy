#include <cmath>
#include <cstdio>
#include <functional>

using std::function;

typedef function<double(double)> Integrand;

typedef function<double(const Integrand&, double, double, size_t)> Integrator;

double trapezoidal(const Integrand& f, const double a, const double b, const size_t N)
{
	const double h = (b - a) / N;
	double sum = 0.;

	for (size_t i = 1; i < N; ++i)
	{
		sum += 2. * f(a + h * i);
	}

	sum += f(a) + f(b);
	sum *= h / 2.;
	return sum;
}

double simpson(const Integrand& f, const double a, const double b, const size_t N)
{
	const double h = (b - a) / N;
	double sum = 0.;

	for (size_t i = 2; i < N; i += 2)
	{
		sum += 2. * f(a + h * i);
	}

	for (size_t i = 1; i < N; i += 2)
	{
		sum += 4. * f(a + h * i);
	}

	sum += f(a) + f(b);
	sum *= h / 3.;
	return sum;
}

double rectangle(const Integrand& f, const double a, const double b, const size_t N)
{
	const double h = (b - a) / N;
	double sum = 0.;

	for (size_t i = 0; i < N; ++i)
	{
		sum += f(a + h * (i + 0.5));
	}

	sum *= h;
	return sum;
}

void work(const Integrator& s, const Integrand& f, const double a, const double b, size_t N, const double epsilon)
{
	double old;
	double current = 0.;

	do
	{
		old = current;
		current = s(f, a, b, N);

		printf("N:       %zu\n", N);
		printf("current: %.6f\n", current);

		N *= 2;
	}
	while (std::abs((old - current) / old) >= epsilon); // or fabs, _not_ just abs!
}

void ex3work(const Integrand& f, const double a, const double b, const double correct)
{
	const size_t N = 2;
	const double epsilon = 1e-4;

	printf("trapezoidal:\n");
	work(trapezoidal, f, a, b, N, epsilon);
	printf("correct: %.6f\n\n", correct);

	printf("Simpson:\n");
	work(simpson, f, a, b, N, epsilon);
	printf("correct: %.6f\n\n", correct);

	printf("rectangle:\n");
	work(rectangle, f, a, b, N, epsilon);
	printf("correct: %.6f\n", correct);
}

void ex3()
{
	printf("I_1:\n");
	ex3work([] (double x) { return exp(-x) / x; }, 1., 100., 0.219384);

	printf("\n\n");

	printf("I_2:\n");
	ex3work([] (double x) { return x * sin(1 / x); }, 1e-10, 1., 0.378530);
}

int main()
{
	ex3();
}
