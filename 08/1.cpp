#include <cstdio>
#include <limits>
#include <random>
#include <string>

using std::string;

// m == 0 ⇒ m = std::numeric_limits<UIntType>::max() + 1u
template <class UIntType, UIntType a, UIntType c, UIntType m>
class linear_congruential_engine
{
public:
	using result_type = UIntType;

private:
	static constexpr result_type Min = c == 0u ? 1u : 0u;
	static constexpr result_type Max = m - 1u;

	static_assert(m == 0u || a < m, "linear_congruential_engine invalid parameters");
	static_assert(m == 0u || c < m, "linear_congruential_engine invalid parameters");
	static_assert(Min < Max,        "linear_congruential_engine invalid parameters");

	result_type x;

public:
	static constexpr result_type multiplier = a;
	static constexpr result_type increment = c;
	static constexpr result_type modulus = m;
	static constexpr result_type default_seed = 1u;

	explicit linear_congruential_engine(result_type s = default_seed)
	{
		seed(s);
	}

	void seed(result_type s = default_seed)
	{
		x = m == 0u ? (c == 0u ? (s == 0     ? 1u : s    ) : s    )
		            : (c == 0u ? (s % m == 0 ? 1u : s % m) : s % m);
	}

	static constexpr result_type min()
	{
		return Min;
	}

	static constexpr result_type max()
	{
		return Max;
	}

	void discard(unsigned long long z)
	{
		for (; z != 0ull; --z)
		{
			(*this)();
		}
	}

	result_type operator()()
	{
		x = m == 0u ? (a * x + c)
		            : (a * x + c) % m;
		return x;
	}
};

// uniform in [0, 1[
template <class RealType = double>
class uniform_real01_distribution
{
public:
	using result_type = RealType;

public:
	constexpr result_type min()
	{
		return 0.0;
	}

	constexpr result_type max()
	{
		return 1.0;
	}

	template <class URNG>
	result_type operator()(URNG& g)
	{
		return result_type(g() - g.min()) / result_type(g.max() - g.min());
	}
};

template <class Engine, class Distribution>
void work(const string& name, uint64_t s)
{
	Engine e(s);
	Distribution d;

	const uint64_t N_max = uint64_t(1e6);
	const uint64_t N = e.max() > N_max ? N_max : e.max();

	auto file = fopen(name.c_str(), "w");

	for (uint64_t i = 0; i < N; ++i)
	{
		fprintf(file, "%.20e\n", d(e));
	}

	fclose(file);
}

int usage(char** argv)
{
	fprintf(stderr, "Usage: %s 1|2|3|4|std\n", argv[0]);
	return 1;
}

int main(int argc, char** argv)
{
	if (argc == 2)
	{
		string arg(argv[1]);
		if (arg == "1")
		{
			work<linear_congruential_engine<uint64_t, 20u, 120u, 6075u>, uniform_real01_distribution<double>>("1-1.dat", 1234u);
		}
		else if (arg == "2")
		{
			work<linear_congruential_engine<uint64_t, 137u, 187u, 256u>, uniform_real01_distribution<double>>("1-2.dat", 1234u);
		}
		else if (arg == "3")
		{
			work<linear_congruential_engine<uint64_t, 65539u, 0u, 2147483648u>, uniform_real01_distribution<double>>("1-3.dat", 123456789u);
		}
		else if (arg == "4")
		{
			work<linear_congruential_engine<uint64_t, 16807u, 0u, 2147483647u>, uniform_real01_distribution<double>>("1-4.dat", 1234u);
		}
		else if (arg == "std")
		{
			work<std::mt19937_64, std::uniform_real_distribution<double>>("1-std.dat", 1234u);
		}
		else
		{
			return usage(argv);
		}
	}
	else
	{
		return usage(argv);
	}
}
