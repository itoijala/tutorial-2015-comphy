#include <cmath>
#include <cstdio>
#include <functional>
#include <random>
#include <string>
#include <tuple>
#include <utility>

using std::mt19937;
using std::string;

template <class RealType = double>
class normal_distribution_box_muller
{
public:
	using result_type = RealType;

private:
	std::uniform_real_distribution<result_type> uniform;

	result_type mu;
	result_type sigma;

	bool spare = false;
	result_type r;
	result_type s;

public:
	explicit normal_distribution_box_muller(result_type mean = 0.0, result_type stddev = 1.0):
	    uniform(-1., std::nextafter(1., 2.)),
	    mu(mean),
	    sigma(stddev)
	{
	}

	constexpr result_type min()
	{
		return - std::numeric_limits<result_type>::infinity();
	}

	constexpr result_type max()
	{
		return + std::numeric_limits<result_type>::infinity();
	}

	result_type mean() const
	{
		return mu;
	}

	result_type stddev() const
	{
		return sigma;
	}

	void reset()
	{
		spare = false;
	}

	template <class URNG>
	result_type operator()(URNG& g)
	{
		result_type x = work(g);
		x *= sqrt(-2. * log(s) / s);
		return x * sigma + mu;
	}

private:
	template <class URNG>
	result_type work(URNG& g)
	{
		if (spare)
		{
			spare = false;
			return r;
		}
		spare = true;

		result_type r1;
		do
		{
			r1 = uniform(g);
			r = uniform(g);
			s = r1 * r1 + r * r;
		}
		while (s >= 1. || !std::isfinite(log(s)));

		return r1;
	}
};

template <class RealType = double, size_t N = 1000u>
class normal_distribution_central_limit
{
public:
	using result_type = RealType;

private:
	std::uniform_real_distribution<result_type> uniform;

	result_type mu;
	result_type sigma;

public:
	explicit normal_distribution_central_limit(result_type mean = 0.0, result_type stddev = 1.0):
	    uniform(0., std::nextafter(1., 2.)),
	    mu(mean),
	    sigma(stddev)
	{
	}

	constexpr result_type min()
	{
		return - std::numeric_limits<result_type>::infinity();
	}

	constexpr result_type max()
	{
		return + std::numeric_limits<result_type>::infinity();
	}

	result_type mean() const
	{
		return mu;
	}

	result_type stddev() const
	{
		return sigma;
	}

	void reset()
	{
	}

	template <class URNG>
	result_type operator()(URNG& g)
	{
		result_type x = 0.;

		for (size_t i = 0; i < N; ++i)
		{
			x += uniform(g);
		}

		x = (x - result_type(N) / 2.) / sqrt(result_type(N) / 12.);
		return x * sigma + mu;
	}
};

template <class RealType, class F, class G>
class rejection_sampling
{
public:
	using result_type = RealType;

private:
	std::uniform_real_distribution<result_type> uniform;

	F f;
	G g;

public:
	rejection_sampling(F f, G g) :
	    uniform(0., 1.),
	    f(f),
	    g(g)
	{
	}

	template <class URNG>
	result_type operator()(URNG& gen)
	{
		result_type x, gx, u;
		do
		{
			std::tie(x, gx) = g(gen);
			u = uniform(gen);
		}
		while (u >= f(x) / gx);
		return x;
	}
};

template <class F, class G>
rejection_sampling<double, F, G> make_double_rejection_sampling(F&& f, G&& g)
{
	return rejection_sampling<double, F, G>(std::forward<F>(f), std::forward<G>(g));
}

template <class RealType = double>
class parabolic_distribution
{
public:
	using result_type = RealType;

private:
	std::uniform_real_distribution<result_type> uniform;

	result_type x0_, a_, b_;

	result_type p1, p2;

public:
	parabolic_distribution(result_type x0, result_type a, result_type b) :
	    uniform(0., 1.),
	    x0_(x0),
	    a_(a),
	    b_(b),
	    p1(pow(a - x0, 3)),
	    p2(pow(b - x0, 3) - p1)
	{
	}

	result_type min() const
	{
		return a_;
	}

	result_type max() const
	{
		return b_;
	}

	result_type x0() const
	{
		return x0_;
	}

	result_type a() const
	{
		return a_;
	}

	result_type b() const
	{
		return b_;
	}

	void reset()
	{
	}

	template <class URNG>
	result_type operator()(URNG& g)
	{
		result_type u = uniform(g);
		return cbrt(u * p2 - p1) + x0_;
	}
};

template <class Distribution>
void work(const string& name, Distribution d)
{
	const size_t N = size_t(1e6);

	mt19937 e;

	auto file = fopen(name.c_str(), "w");

	for (size_t i = 0; i < N; ++i)
	{
		fprintf(file, "%.20e\n", d(e));
	}

	fclose(file);
}

int usage(char** argv)
{
	fprintf(stderr, "Usage: %s a|b|c|d\n", argv[0]);
	return 1;
}

int main(int argc, char** argv)
{
	if (argc == 2)
	{
		string arg(argv[1]);
		if (arg == "a")
		{
			work("2-a.dat", normal_distribution_box_muller<>(3., 2.));
		}
		else if (arg == "b")
		{
			work("2-b.dat", normal_distribution_central_limit<>(3., 2.));
		}
		else if (arg == "c")
		{
			std::uniform_real_distribution<> u(0., M_PI);
			work("2-c.dat", make_double_rejection_sampling(
				[] (double x)
				{
					return 0.5 * sin(x);
				},
				[&u] (auto& g)
				{
					return std::make_tuple(u(g), 1.);
				}
			));
		}
		else if (arg == "d")
		{
			work("2-d.dat", parabolic_distribution<>(0., 0., 1.));
		}
		else
		{
			return usage(argv);
		}
	}
	else
	{
		return usage(argv);
	}
}
