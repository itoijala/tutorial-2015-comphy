#!/usr/bin/env python3

import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats

dists = {
    'a': (-5, 11,    lambda x: scipy.stats.norm(loc=3, scale=2).pdf(x)),
    'b': (-5, 11,    lambda x: scipy.stats.norm(loc=3, scale=2).pdf(x)),
    'c': ( 0, np.pi, lambda x: 0.5 * np.sin(x)),
    'd': ( 0,  1,    lambda x: 3 * x**2),
}

a, b, p = dists[sys.argv[1].rsplit('.', 1)[0].rsplit('-', 1)[1]]

y = np.linspace(a, b, 1000)

x = np.loadtxt(sys.argv[1])

hist, bins = np.histogram(x, bins=100, density=True)

fig, ax = plt.subplots(1, 1)

ax.plot(bins, list(hist) + [hist[-1]], drawstyle='steps-post', color='k', solid_capstyle='butt')

ax.plot(y, p(y), 'b')

fig.savefig('{}.pdf'.format(sys.argv[1].rsplit('.', 1)[0]))
