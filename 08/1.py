#!/usr/bin/env python3

import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

x = np.loadtxt(sys.argv[1])

fig = plt.figure()
fig.set_figheight(4 * fig.get_figheight())

hist, bins = np.histogram(x, bins=10, range=(0, 1))

ax1 = fig.add_subplot(4, 1, 1)
ax1.bar(bins[:-1], hist, width=np.diff(bins))

ax1.set_xlabel(r'$x$')
ax1.set_ylabel(r'$N$')

ax2 = fig.add_subplot(4, 1, 2)
ax2.bar(bins[:-1], hist - np.mean(hist), width=np.diff(bins))

ax2.set_xlabel(r'$x$')
ax2.set_ylabel(r'$N - \langle N \rangle$')

y = x
if len(y) > 1e4:
    y = y[:1e4]
if len(x) % 2 != 0:
    y = y[:-(len(y) % 2)]

s2 = np.array(np.split(y, len(y) // 2))

ax3 = fig.add_subplot(4, 1, 3)
ax3.scatter(s2[:,0], s2[:,1])

y = x
if len(y) > 1e4:
    y = y[:1e4]
if len(x) % 3 != 0:
    y = y[:-(len(y) % 3)]

s3 = np.array(np.split(y, len(y) // 3))

ax4 = fig.add_subplot(4, 1, 4, projection='3d')
ax4.scatter(s3[:,0], s3[:,1], s3[:,2])

p = {
    '1': (  20,  11),
    '2': (  61, -43),
    '3': (-153,  59),
}

ax4.view_init(*p.get(sys.argv[1].rsplit('.', 1)[0].rsplit('-', 1)[1], (None, None)))

fig.savefig('{}.pdf'.format(sys.argv[1].rsplit('.', 1)[0]))
