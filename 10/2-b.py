#!/usr/bin/env python3

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize
import scipy.stats

def curve_fit(f, x, y, **kwargs):
    popt, pcov = scipy.optimize.curve_fit(f, x, y, **kwargs)
    if len(np.shape(pcov)) == 0 or np.any(np.isinf(pcov)):
        print("popt =", popt)
        print("pcov =", pcov)
        raise Exception("Fit unsuccessful!")
    return popt

π = np.loadtxt('2-b.dat', skiprows=1)

hist, bins = np.histogram(π, bins=1000)

def f(x, a, μ, σ):
    return a * scipy.stats.norm.pdf(x, loc=μ, scale=σ)

i = np.where(hist > 0)

a, μ, σ = curve_fit(f, (bins[:-1] + 0.5 * np.diff(bins))[i], hist[i])

fig, ax = plt.subplots(1, 1)

ax.plot(bins, list(hist) + [hist[-1]], drawstyle='steps-post', color='k', solid_capstyle='butt')

x = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)

ax.plot(x, f(x, a, μ, σ), 'b-')

ax.set_xlabel(r'$\pi$')
ax.set_ylabel(r'\# per bin')

ax.text(2.85, 33000, r'$a = {}, \mu = {}, \sigma = {}$'.format(a, μ, σ))

fig.savefig('2-b.pdf')
