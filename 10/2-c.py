#!/usr/bin/env python3

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

a, A = np.loadtxt('2-c.dat', unpack=True)

fig, ax = plt.subplots(1, 1)

ax.plot(a, A, 'b-')

ax.set_xlabel(r'$a/b$')
ax.set_ylabel(r'$A$')

fig.savefig('2-c.pdf')
