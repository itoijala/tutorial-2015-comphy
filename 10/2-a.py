#!/usr/bin/env python3

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

def linregress(x, y):
    N = len(y)
    Δ = N * np.sum(x**2) - (np.sum(x))**2

    A = (N * np.sum(x * y) - np.sum(x) * np.sum(y)) / Δ
    B = (np.sum(x**2) * np.sum(y) - np.sum(x) * np.sum(x * y)) / Δ

    return A, B

N, π = np.loadtxt('2-a.dat', skiprows=1, unpack=True)
δ = np.abs(π - np.pi)

A, B = linregress(np.log(N), np.log(δ))

fig, ax = plt.subplots(1, 1)

x = np.linspace(N[0], N[-1])
ax.plot(x, np.exp(A * np.log(x) + B), 'b-')

ax.plot(N, δ, 'rx')

ax.set_xscale('log')
ax.set_yscale('log')

ax.set_xlabel(r'$N$')
ax.set_ylabel(r'$\delta$')

ax.text(1e2, 5e-1, r'$A = {}, B = {}$'.format(A, B))

fig.savefig('2-a.pdf')
