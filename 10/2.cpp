#include <cmath>
#include <cstdio>
#include <limits>
#include <random>
#include <vector>
#include <string>

#include <omp.h>

#include <Eigen/Core>

template <int d>
using Vector = Eigen::Matrix<double, d, 1>;

Vector<1> v1(const double x)
{
	Vector<1> v;
	v << x;
	return v;
}

template<int dout, int din, class F>
Vector<dout> integrate(const F& f, const Vector<din>& limit1, const Vector<din>& limit2, const size_t N, const std::mt19937::result_type seed)
{
	#pragma omp declare reduction(+ : Vector<dout> : omp_out += omp_in) initializer(omp_priv = Vector<dout>::Zero())

	Vector<dout> sum = Vector<dout>::Zero();

	#pragma omp parallel
	{
		std::mt19937 gen(static_cast<std::mt19937::result_type>(omp_get_thread_num()) + seed);

		std::vector<std::uniform_real_distribution<double>> dists(din);
		for (int i = 0; i < din; ++i)
		{
			dists[size_t(i)] = std::uniform_real_distribution<>(limit1[i], std::nextafter(limit2[i], std::numeric_limits<double>::infinity()));
		}

		#pragma omp for reduction(+:sum)
		for (size_t n = 0; n < N; ++n)
		{
			Vector<din> x = Vector<din>::LinSpaced(0, din - 1).unaryExpr(
			    [&gen, &dists] (double x) { return dists[size_t(x)](gen); }
			);
			sum += f(x);
		}
	}

	return sum;
}

template <int dout, int din, class F, class G>
Vector<dout> iterate(const F& f, const G& g, const Vector<din>& limit1, const Vector<din>& limit2, const double epsilon)
{
	size_t N = 1024;
	Vector<dout> old;
	Vector<dout> result = g(integrate<dout, din, F>(f, limit1, limit2, N, 0), N);

	fprintf(stderr, "# N old new δ\n");

	do
	{
		old = result;
		result = 0.5 * (g(integrate<dout, din, F>(f, limit1, limit2, N, N), N) + result);

		fprintf(stderr, "%zu %.20e %.20e %.20e\n", N, old[0], result[0], ((result - old).array().abs() / (result + old).array().abs()).maxCoeff());

		N *= 2;
	}
	while (((result - old).array().abs() >= epsilon * (result + old).array().abs()).any());

	return result;
}

void work_a()
{
	Vector<2> limit1(-1., -1.);
	Vector<2> limit2(1., 1.);
	const double V = (limit2 - limit1).prod();

	printf("# N π\n");

	for (size_t n = 1, N = 10; n < 10; ++n, N *= 10)
	{
		Vector<1> result = integrate<1>(
		    [] (const Vector<2>& x) { return v1(x.squaredNorm() <= 1. ? 1. : 0.); },
		    limit1, limit2, N, N
		);

		printf("%zu %.20e\n", N, V * result[0] / double(N));
	}
}

void work_b()
{
	const size_t n = 1e6;
	const size_t N = 1e3;

	Vector<2> limit1(-1., -1.);
	Vector<2> limit2(1., 1.);
	const double V = (limit2 - limit1).prod();

	printf("# π\n");

	for (size_t i = 0; i < n; ++i)
	{
		Vector<1> result = integrate<1>(
		    [] (const Vector<2>& x) { return v1(x.squaredNorm() <= 1. ? 1. : 0.); },
		    limit1, limit2, N, size_t(omp_get_num_procs()) * i
		);

		printf("%.20e\n", V * result[0] / double(N));
	}
}

void work_c()
{
	const double b = 1.;

	printf("# a A\n");

	for (size_t i = 0; i < 100; ++i)
	{
		const double a = double(i) + 1.;

		Vector<2> limit1(-a, -b);
		Vector<2> limit2(a, b);
		const double V = (limit2 - limit1).prod();

		Vector<1> result = iterate<1>(
		    [limit2] (const Vector<2>& x) { return v1(x.cwiseQuotient(limit2).squaredNorm() <= 1. ? 1. : 0.); },
		    [V] (const Vector<1>& x, const size_t N) { return V * x / double(N); },
		    limit1, limit2, 1e-3
		);

		printf("%.20e %.20e\n", a, result[0]);
	}
}

void work_d()
{
	const double a = sqrt(2.);
	const double b = 1.;

	Vector<2> limit1(-a, -b);
	Vector<2> limit2(a, b);
	const double V = (limit2 - limit1).prod();

	Vector<1> result = iterate<1>(
	    [limit2] (const Vector<2>& x) { return v1(x.cwiseQuotient(limit2).squaredNorm() <= 1. ? exp(- pow(x[0], 2)) : 0.); },
	    [V] (const Vector<1>& x, const size_t N) { return V * x / double(N); },
	    limit1, limit2, 1e-5
	);

	printf("I = %.20e\n", result[0]);
}

int usage(char** argv)
{
	fprintf(stderr, "Usage: %s a|b|c|d\n", argv[0]);
	return 1;
}

int main(int argc, char** argv)
{
	Eigen::initParallel();

	if (argc == 2)
	{
		std::string a(argv[1]);
		if (a == "a")
		{
			work_a();
		}
		else if (a == "b")
		{
			work_b();
		}
		else if (a == "c")
		{
			work_c();
		}
		else if (a == "d")
		{
			work_d();
		}
		else
		{
			return usage(argv);
		}
	}
	else
	{
		return usage(argv);
	}
}
