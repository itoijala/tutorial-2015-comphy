#!/usr/bin/env python3

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import numpy.random
import scipy.stats

def linregress(x, y):
    N = len(y)
    Δ = N * np.sum(x**2) - (np.sum(x))**2

    A = (N * np.sum(x * y) - np.sum(x) * np.sum(y)) / Δ
    B = (np.sum(x**2) * np.sum(y) - np.sum(x) * np.sum(x * y)) / Δ

    return A, B

def walk(a, N, W):
    r = np.zeros((W, 2))
    for _ in range(N):
        r += 2 * a * np.random.random_sample(np.shape(r)) - a
    return r

def means(a, Ns, W):
    r = np.array([walk(a, N, W) for N in Ns])

    x = np.mean(r[:,:,0], axis=1)
    y = np.mean(r[:,:,1], axis=1)
    r2 = np.mean(np.sum(r**2, axis=2), axis=1)

    return x, y, r2

def plot_means(a, Ns, W):
    Ns = np.array(Ns)
    x, y, r2 = means(a, Ns, W)

    D4, b = linregress(Ns, r2)
    D = 0.25 * D4

    fig, (ax1, ax2) = plt.subplots(2, 1)
    fig.set_figheight(2 * fig.get_figheight())

    ax1.plot(Ns, x, 'rx')
    ax1.plot(Ns, y, 'bx')

    x = np.linspace(0, np.max(Ns))

    ax2.plot(x, 4 * D * x + b, 'b-')
    ax2.plot(Ns, r2, 'rx')

    ax2.text(1e2, 6e2, r'$\displaystyle D = {} \approx \frac{{1}}{{6}} \frac{{a^2}}{{\tau}}$'.format(D))

    fig.savefig('1-a.pdf')

def plot_hist(a, N, W, lim):
    r = walk(a, N, W)

    hist, bins = np.histogramdd(r, bins=50)

    x, y = np.mgrid[-lim:lim:50j, -lim:lim:50j]

    z = np.empty(x.shape + (2,))
    z[:,:,0] = x
    z[:,:,1] = y
    z = N * scipy.stats.multivariate_normal(mean=[0, 0], cov=np.identity(2) * 2/3 * N * a**2).pdf(z)

    fig, (ax1, ax2) = plt.subplots(2, 1)
    fig.set_figheight(2 * fig.get_figheight())

    cmap = mpl.cm.get_cmap('Blues')
    cmap.set_under('w')

    ax1.pcolor(bins[0], bins[1], hist, cmap=cmap, vmin=1)

    ax1.set_aspect('equal', adjustable='box')
    ax1.set_xlim(-lim, lim)
    ax1.set_ylim(-lim, lim)

    ax2.pcolor(x[:,0], y[0], z, cmap=cmap)

    ax2.set_aspect('equal', adjustable='box')
    ax2.set_xlim(-lim, lim)
    ax2.set_ylim(-lim, lim)

    fig.savefig('1-c-{}.pdf'.format(N))

a = 1
W = int(1e5)

plot_means(a, [10, 50, 100, 500, 1000], W)

for N, lim in [(10, 10), (100, 30), (1000, 100)]:
    plot_hist(a, N, W, lim)
